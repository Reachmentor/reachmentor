<?php
 
$messages = array(
    'messages' => array(
        array(
            'number' => 9600735864,
            'text' => rawurlencode('This is your message')
        ),
        array(
            'number' => 8675575150,
            'text' => rawurlencode('This is another message')
        )
    )
);
 
// Prepare data for POST request
$data = array(
    'apikey' => '6oq77NzO3aY-PIo8apv3vuVGi87XTMIkghxrwQ4ANL',
    'data' => json_encode($messages)
);
 
// Send the POST request with cURL
$ch = curl_init('https://api.textlocal.in/bulk_json/');
curl_setopt($ch, CURLOPT_POST, true);
curl_setopt($ch, CURLOPT_POSTFIELDS, $data);
curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
$response = curl_exec($ch);
curl_close($ch);
 
echo $response;