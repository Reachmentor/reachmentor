<?php

/**

 * The Header for our theme.

 *

 * Displays all of the <head> section and everything up till <div id="content">

 *

 * @package thim

 */

?><!DOCTYPE html>

<html itemscope itemtype="http://schema.org/WebPage" <?php language_attributes(); ?>>

<head>
<meta name=�google-site-verification� content=�5p90rlB64Erl9O1LKwBG3MUjGRCXbzaLKiGznQksDFU� />
<meta charset="UTF-8">

<meta  name="description"  content="Reachmentor� : Upskill your career with our future technology courses. Learn Programming in Python bot creation,Data Science,E-commerce,machine Learning,Devops,NodeJs,MeanStack,Drupal,Wordpress,Magento & Amazon Web services" />

<meta  name="keywords"  content=" Learn courses online PHP,Python bot,Data Science,E-commerce,Machine Learning,NodeJs,Drupal,Wordpress & Magento Development" />



	<meta charset="<?php bloginfo( 'charset' ); ?>">

	<meta name="viewport" content="width=device-width, initial-scale=1">

	<link rel="profile" href="http://gmpg.org/xfn/11">

	<link rel="pingback" href="<?php esc_url( bloginfo( 'pingback_url' ) ); ?>">

	<?php wp_head(); ?>

                                         
<script>!function(e,t,a){var c=e.head||e.getElementsByTagName("head")[0],n=e.createElement("script");n.async=!0,n.defer=!0, n.type="text/javascript",n.src=t+"/static/js/chat_widget.js?config="+JSON.stringify(a),c.appendChild(n)}(document,"https://app.engati.com",{bot_key:"50010ddab30e4202",welcome_msg:true,branding_key:"default",server:"https://app.engati.com",e:"p" });</script>

<!-- Global site tag (gtag.js) - Google Analytics -->
<script async src=�https://www.googletagmanager.com/gtag/js?id=UA-133909191-1�></script>
<script>
 window.dataLayer = window.dataLayer || [];
 function gtag(){dataLayer.push(arguments);}
 gtag(�js�, new Date());

 gtag(�config�, �UA-133909191-1�);
</script>
<meta name=�google-site-verification� content=�5p90rlB64Erl9O1LKwBG3MUjGRCXbzaLKiGznQksDFU� />
</head>

<body <?php body_class(); ?> id="thim-body">



<?php do_action( 'thim_before_body' ); ?>



<div id="wrapper-container" class="wrapper-container">

	<div class="content-pusher">

		<header id="masthead" class="site-header affix-top<?php thim_header_class(); ?>">

			<?php

			//Toolbar

			if ( get_theme_mod( 'thim_toolbar_show', true ) ) {

				get_template_part( 'inc/header/toolbar' );

			}



			//Header style

			if ( get_theme_mod( 'thim_header_style', 'header_v1' ) ) {

				get_template_part( 'inc/header/' . get_theme_mod( 'thim_header_style', 'header_v1' ) );

			}



			?>

		</header>

		<!-- Mobile Menu-->

		<nav class="mobile-menu-container mobile-effect">

			<?php get_template_part( 'inc/header/menu-mobile' ); ?>

		</nav>

		<div id="main-content">