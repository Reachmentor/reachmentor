<?php
/**
 * Template for displaying the students of a course
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 2.1.4
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$course = LP()->global['course'];
if ( !$course || !$course->is_require_enrollment() ) {
	return;
}

$count = $course->count_users_enrolled( 'append' ) ? $course->count_users_enrolled( 'append' ) : 0;
?>
<div class="course-students">
	<label><?php esc_html_e( 'Students', 'eduma' ); ?></label>

	<div class="value" style="margin-top: -8px;
    font-size: 15px;">
    <i class="fa fa-group" style="color:#8d8d8d"></i>
		<?php
		echo "<br>ProfileViews<br>"; 
		echo '<div style= "color:#29aeb5 ; font-size:18px"><b>';
		echo esc_html( $count ); ?></b></div>
	</div>

</div>