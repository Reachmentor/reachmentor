<?php
/**
 * Template for displaying the tags of a course
 *
 * @author  ThimPress
 * @package LearnPress/Templates
 * @version 1.0
 */

if ( !defined( 'ABSPATH' ) ) {
	exit; // Exit if accessed directly
}

$course = LP()->global['course'];

$tags = apply_filters( 'learn_press_course_tags', get_the_term_list( $course->id, 'course_tag', __( '', 'learnpress' ), ', ', '' ) );
if ( !$tags ) {
	return;
}
?>
<div class="course-wishlist-box">
<?php if(stristr($tags,"private")) {?>
<span style="padding:8px;opacity:10%;background-color:#33AFFF;color:#fff;font-weight:bold">Private</span>
<?php } ?>
<?php if(stristr($tags,"group")) {?>
<span style="padding:8px;opacity:10%;background-color:#C4FF33;color:#fff;font-weight:bold">Group</span>
<?php } ?>
<?php if(stristr($tags,"workshop")) {?>
<span style="padding:8px;opacity:10%;background-color:#FF5733;color:#fff;font-weight:bold">Workshop</span>
<?php } ?></div>
