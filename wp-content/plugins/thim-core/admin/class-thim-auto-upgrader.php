<?php

/**
 * Class Thim_Theme_Upgrader.
 *
 * @since 0.9.0
 */
class Thim_Auto_Upgrader extends Thim_Singleton {
	/**
	 * @since 1.1.0
	 *
	 * @var null
	 */
	private $api_check_update_plugins = null;

	/**
	 * Thim_Theme_Upgrader constructor.
	 *
	 * @since 0.9.0
	 */
	protected function __construct() {
		$this->api_check_update_plugins = Thim_Admin_Config::get( 'api_check_update_plugins' );

		$this->init_hooks();
	}

	/**
	 * Init hooks.
	 *
	 * @since 0.9.0
	 */
	private function init_hooks() {
		add_filter( 'http_request_args', array( $this, 'exclude_check_update_themes_from_wp_org' ), 100, 2 );
		add_filter( 'http_request_args', array( $this, 'exclude_check_update_plugins_from_wp_org' ), 100, 2 );

		add_filter( 'pre_set_site_transient_update_themes', array( $this, 'inject_update_themes' ), 100 );
		add_filter( 'pre_set_transient_update_themes', array( $this, 'inject_update_themes' ), 1000 );

		add_filter( 'pre_set_site_transient_update_plugins', array( $this, 'inject_update_plugins' ), 100 );
		add_filter( 'pre_set_transient_update_plugins', array( $this, 'inject_update_plugins' ), 100 );

		add_filter( 'upgrader_package_options', array( $this, 'pre_update_theme' ), 100 );

		add_filter( 'upgrader_pre_download', array( $this, 'pre_filter_download_plugin' ), 100, 3 );
	}

	/**
	 * Inject filter download plugin.
	 *
	 * @since 1.1.0
	 *
	 * @param $reply
	 * @param $package
	 * @param $updater WP_Upgrader
	 *
	 * @return bool
	 */
	public function pre_filter_download_plugin( $reply, $package, $updater ) {
		if ( is_wp_error( $reply ) && ! empty( $package ) ) {//Override Visual Composer
			return false;
		}

		return $reply;
	}

	/**
	 * Pre update theme, get again link download theme.
	 *
	 * @since 0.8.0
	 *
	 * @param $options
	 *
	 * @return mixed
	 */
	public function pre_update_theme( $options ) {
		$hook_extra = isset( $options['hook_extra'] ) ? $options['hook_extra'] : false;

		if ( ! $hook_extra ) {
			return $options;
		}

		$theme = isset( $hook_extra['theme'] ) ? $hook_extra['theme'] : false;

		if ( ! $theme ) {
			return $options;
		}

		$themes = Thim_Product_Registration::get_themes();
		foreach ( $themes as $stylesheet => $data ) {
			if ( $theme == $stylesheet ) {
				if ( $url_download = Thim_Product_Registration::get_url_download_theme( $stylesheet ) ) {
					$options['package'] = $url_download;
				}

				return $options;
			}
		}

		return $options;
	}

	/**
	 * Add filter update plugins.
	 *
	 * @since 1.0.0
	 *
	 * @param $value
	 *
	 * @return mixed
	 */
	public function inject_update_plugins( $value ) {
		$response = ! empty( $value->response ) ? $value->response : array();

		$plugin_checker = new Thim_Plugin_Check_Update( $this->api_check_update_plugins );
		$plugins        = Thim_Plugins_Manager::get_external_plugins();

		foreach ( $plugins as $index => $plugin ) {
			$plugin_file = $plugin->get_plugin_file();

			$update = $plugin_checker->check_can_update( $plugin );
			if ( ! $update ) {
				if ( isset( $response[ $plugin_file ] ) ) {
					unset( $response[ $plugin_file ] );
				}
				continue;
			}

			$object              = new stdClass();
			$object->slug        = $plugin->get_slug();
			$object->plugin      = $plugin_file;
			$object->new_version = $update['version'];
			$object->url         = $update['homepage'];
			$object->package     = $update['download_link'];
			$object->tested      = $update['tested'];

			$response[ $plugin_file ] = $object;
		}

		$value->response = $response;

		return $value;
	}

	/**
	 * Add filter update theme.
	 *
	 * @since 0.7.0
	 *
	 * @param $value
	 *
	 * @return mixed
	 */
	public function inject_update_themes( $value ) {
		$detect_ajax_update = isset( $_REQUEST['action'] ) ? $_REQUEST['action'] : 'no';

		if ( $detect_ajax_update !== 'thim_core_update_theme' ) {
			return $value;
		}

		$theme_data    = Thim_Theme_Manager::get_metadata();
		$template      = $theme_data['template'];
		$update_themes = Thim_Product_Registration::get_update_themes();
		$themes        = $update_themes['themes'];

		$data = isset( $themes[ $template ] ) ? $themes[ $template ] : false;
		if ( ! $data ) {
			return $value;
		}

		$value->response[ $template ] = array(
			'theme'       => $template,
			'new_version' => $data['version'],
			'package'     => '',
			'url'         => $data['url']
		);

		return $value;
	}

	/**
	 * Exclude check plugins update from wp.org.
	 *
	 * @since 1.1.0
	 *
	 * @param $request
	 * @param $url
	 *
	 * @return mixed
	 */
	public function exclude_check_update_plugins_from_wp_org( $request, $url ) {
		if ( false === strpos( $url, '//api.wordpress.org/plugins/update-check/1.1/' ) ) {
			return $request;
		}

		$data    = json_decode( $request['body']['plugins'] );
		$plugins = $this->get_exclude_plugins_update();
		foreach ( $plugins as $index => $plugin_file ) {
			if ( isset( $data->plugins->$plugin_file ) ) {
				unset( $data->plugins->$plugin_file );
			}
		}

		$request['body']['plugins'] = wp_json_encode( $data );

		return $request;
	}

	/**
	 * Exclude check themes update from wp.org.
	 *
	 * @since 0.9.0
	 *
	 * @param $request
	 * @param $url
	 *
	 * @return mixed
	 */
	public function exclude_check_update_themes_from_wp_org( $request, $url ) {
		if ( false === strpos( $url, '//api.wordpress.org/themes/update-check/1.1/' ) ) {
			return $request;
		}

		$data   = json_decode( $request['body']['themes'] );
		$themes = Thim_Product_Registration::get_themes();
		foreach ( $themes as $slug => $theme ) {
			if ( isset( $data->themes->$slug ) ) {
				unset( $data->themes->$slug );
			}
		}

		$request['body']['themes'] = wp_json_encode( $data );

		return $request;
	}

	/**
	 * Get list exclude check update plugins.
	 *
	 * @since 1.1.0
	 *
	 * @since array
	 */
	private function get_exclude_plugins_update() {
		$plugins = Thim_Plugins_Manager::get_external_plugins();

		$exclude_plugins = array();
		foreach ( $plugins as $index => $plugin ) {
			$plugin_file = $plugin->get_plugin_file();

			$exclude_plugins[] = $plugin_file;
		}

		return apply_filters( 'thim_core_exclude_plugins_check_update', $exclude_plugins );
	}
}