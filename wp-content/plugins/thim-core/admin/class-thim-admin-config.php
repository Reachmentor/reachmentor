<?php

/**
 * Class Thim_Admin_Config
 *
 * @since 1.1.0
 */
class Thim_Admin_Config extends Thim_Singleton {
	/**
	 * @since 1.1.0
	 *
	 * @var null
	 */
	private static $configs = null;

	/**
	 * Thim_Admin_Config constructor.
	 */
	protected function __construct() {
		$this->set_config();
	}

	/**
	 * Set configs.
	 *
	 * @since 1.1.0
	 */
	private function set_config() {
		self::$configs = array(
			'api_check_update_plugins' => 'https://plugins.thimpress.com/downloads/data/update-check.json',
			'personal_token'           => 'lfCHoHSKA6DZD4BiwWo9HRZUknLi3sVm'
		);
	}

	/**
	 * Get config by key.
	 *
	 * @since 1.1.0
	 *
	 * @param $key
	 * @param null $default
	 *
	 * @return mixed|null
	 */
	public static function get( $key, $default = null ) {
		if ( ! isset( self::$configs[ $key ] ) ) {
			return $default;
		}

		return apply_filters( "thim_core_ac_$key", self::$configs[ $key ] );
	}
}