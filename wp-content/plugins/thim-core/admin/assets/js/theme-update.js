(function ($) {
    'use strict';

    $(document).ready(function () {
        var thim_data = thim_theme_update;
        var url_ajax = thim_data.admin_ajax;
        var action = thim_data.action;
        var i18l = thim_theme_update.i18l;

        $(document).on('click', '.tc-login-envato', function (e) {
            e.preventDefault();

            var $btn = $('.tc-registration-wrapper .activate-btn');
            if ($btn.length) {
                $btn.click();
            }
        });

        $(document).on('click', '.tc-update-now', function () {
            var $button = $(this);
            var $notice = $button.closest('.notice');
            var $p = $notice.find('p');

            $notice.removeClass('update-message').addClass('updating-message');
            $p.text(i18l.updating);

            request_update_theme()
                .success(
                    function (response) {
                        var success = response.success || false;
                        var messages = response.data;

                        if (success) {
                            var version = response.data;
                            $('.tc-box-update-wrapper .version-number').text(version);
                            $('.tc-header .version').text(version);
                            $p.html(i18l.updated);
                        } else if (messages.length && !success) {
                            var html = '';
                            messages.forEach(function (string) {
                                html += '<div>' + string + '</div>';
                            });

                            $p.html(html);
                        } else {
                            $p.html(i18l.wrong);
                        }

                        if (success) {
                            $notice.addClass('notice-success');
                        } else {
                            $notice.addClass('notice-error');
                        }
                    }
                )
                .error(
                    function (error) {
                        $p.html(i18l.wrong);
                        $notice.addClass('notice-error');
                    }
                )
                .complete(
                    function () {
                        window.onbeforeunload = null;
                        $notice.removeClass('updating-message').removeClass('notice-warning');
                    }
                );
        });

        function request_update_theme() {
            window.onbeforeunload = function () {
                return i18l.warning_leave;
            };

            return $.ajax({
                url: url_ajax,
                method: 'POST',
                data: {
                    action: action
                },
                dataType: 'json'
            });
        }
    });
})(jQuery);