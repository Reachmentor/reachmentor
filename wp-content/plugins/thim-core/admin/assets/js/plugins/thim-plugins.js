;'use strict';

var Thim_Plugins = (function ($) {
    var ajax_url = thim_plugins_manager.admin_ajax_action;

    return {
        request: request
    };

    function request(action, slug) {
        return $
            .ajax({
                url: ajax_url,
                method: 'POST',
                data: {
                    action: 'thim_plugins_manager',
                    plugin_action: action,
                    slug: slug
                },
                dataType: 'json'
            })
            .error(function (error) {
                console.log(error);
                alert('Something went wrong! Please try again!');
                document.location.reload();
            });
    }
})(jQuery);

var Thim_Plugins_Queue = (function () {
    var actions = [];
    var is_running = false;
    var callback = false;

    return {
        push: push,
        complete: complete
    };

    /**
     * Add callback function when action complete.
     * @param cb
     */
    function complete(cb) {
        callback = cb;
    }

    /**
     * Push to queue actions
     * @param object
     */
    function push(object) {
        actions.push(object);

        if (!is_running) {
            _run();
        }
    }

    /**
     * Run action
     * @private
     */
    function _run() {
        is_running = true;

        if (!actions.length) {
            is_running = false;
            return;
        }
        var first = actions[0];

        Thim_Plugins.request(first.action, first.slug)
            .success(function (response) {
                if (callback) {
                    callback(response);
                }
            })
            .complete(function () {
                _next();
            });
    }

    /**
     * Next action
     * @private
     */
    function _next() {
        is_running = false;
        actions.shift();

        _run();
    }
})();