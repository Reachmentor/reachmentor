<?php

/**
 * Class Thim_Child_Theme_Support
 *
 * @since 1.0.3
 */
if ( ! class_exists( 'Thim_Child_Theme_Support' ) ) {
	class Thim_Child_Theme_Support extends Thim_Singleton {
		/**
		 * Get is child theme.
		 *
		 * @since 1.0.3
		 *
		 * @return bool
		 */
		public function is_child_theme() {
			$stylesheet = get_stylesheet();
			$template   = get_template();

			if ( $stylesheet == $template ) {
				return false;
			}

			return true;
		}

		/**
		 * Thim_Child_Theme_Support constructor.
		 *
		 * @since 1.0.3
		 */
		protected function __construct() {
			$this->init_hooks();
		}

		/**
		 * Init hooks.
		 *
		 * @since 1.0.3
		 */
		private function init_hooks() {
			add_action( 'switch_theme', array( $this, 'switch_theme_update_mods' ) );
		}

		/**
		 * Update theme mods when switch theme.
		 *
		 * @since 1.0.3
		 */
		public function switch_theme_update_mods() {
			if ( ! $this->is_child_theme() ) {
				return;
			}

			if ( get_theme_mod( 'thim_core_extend_parent_theme', false ) ) {
				return;
			}

			$mods = get_option( 'theme_mods_' . get_option( 'template' ) );

			if ( false === $mods ) {
				return;
			}

			foreach ( (array) $mods as $mod => $value ) {
				set_theme_mod( $mod, $value );
			}

			set_theme_mod( 'thim_core_extend_parent_theme', true );
		}
	}
}