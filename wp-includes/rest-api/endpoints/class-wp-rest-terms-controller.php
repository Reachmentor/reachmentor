<?php
/**
 * REST API: WP_REST_Terms_Controller class
 *
 * @package WordPress
 * @subpackage REST_API
 * @since 4.7.0
 */

/**
 * Core class used to managed terms associated with a taxonomy via the REST API.
 *
 * @since 4.7.0
 *
 * @see WP_REST_Controller
 */
class WP_REST_Terms_Controller extends WP_REST_Controller {

	/**
	 * Taxonomy key.
	 *
	 * @since 4.7.0
	 * @access protected
	 * @var string
	 */
	protected $taxonomy;

	/**
	 * Instance of a term meta fields object.
	 *
	 * @since 4.7.0
	 * @access protected
	 * @var WP_REST_Term_Meta_Fields
	 */
	protected $meta;

	/**
	 * Column to have the terms be sorted by.
	 *
	 * @since 4.7.0
	 * @access protected
	 * @var string
	 */
	protected $sort_column;

	/**
	 * Number of terms that were found.
	 *
	 * @since 4.7.0
	 * @access protected
	 * @var int
	 */
	protected $total_terms;

	/**
	 * Constructor.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param string $taxonomy Taxonomy key.
	 */
	public function __construct( $taxonomy ) {
		$this->taxonomy = $taxonomy;
		$this->namespace = 'wp/v2';
		$tax_obj = get_taxonomy( $taxonomy );
		$this->rest_base = ! empty( $tax_obj->rest_base ) ? $tax_obj->rest_base : $tax_obj->name;

		$this->meta = new WP_REST_Term_Meta_Fields( $taxonomy );
	}

	/**
	 * Registers the routes for the objects of the controller.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @see register_rest_route()
	 */
	public function register_routes() {

		register_rest_route( $this->namespace, '/' . $this->rest_base, array(
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_items' ),
				'permission_callback' => array( $this, 'get_items_permissions_check' ),
				'args'                => $this->get_collection_params(),
			),
			array(
				'methods'             => WP_REST_Server::CREATABLE,
				'callback'            => array( $this, 'create_item' ),
				'permission_callback' => array( $this, 'create_item_permissions_check' ),
				'args'                => $this->get_endpoint_args_for_item_schema( WP_REST_Server::CREATABLE ),
			),
			'schema' => array( $this, 'get_public_item_schema' ),
		) );

		register_rest_route( $this->namespace, '/' . $this->rest_base . '/(?P<id>[\d]+)', array(
			'args' => array(
				'id' => array(
					'description' => __( 'Unique identifier for the term.' ),
					'type'        => 'integer',
				),
			),
			array(
				'methods'             => WP_REST_Server::READABLE,
				'callback'            => array( $this, 'get_item' ),
				'permission_callback' => array( $this, 'get_item_permissions_check' ),
				'args'                => array(
					'context' => $this->get_context_param( array( 'default' => 'view' ) ),
				),
			),
			array(
				'methods'             => WP_REST_Server::EDITABLE,
				'callback'            => array( $this, 'update_item' ),
				'permission_callback' => array( $this, 'update_item_permissions_check' ),
				'args'                => $this->get_endpoint_args_for_item_schema( WP_REST_Server::EDITABLE ),
			),
			array(
				'methods'             => WP_REST_Server::DELETABLE,
				'callback'            => array( $this, 'delete_item' ),
				'permission_callback' => array( $this, 'delete_item_permissions_check' ),
				'args'                => array(
					'force' => array(
						'type'        => 'boolean',
						'default'     => false,
						'description' => __( 'Required to be true, as terms do not support trashing.' ),
					),
				),
			),
			'schema' => array( $this, 'get_public_item_schema' ),
		) );
	}

	/**
	 * Checks if a request has access to read terms in the specified taxonomy.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return bool|WP_Error True if the request has read access, otherwise false or WP_Error object.
	 */
	public function get_items_permissions_check( $request ) {
		$tax_obj = get_taxonomy( $this->taxonomy );
		if ( ! $tax_obj || ! $this->check_is_taxonomy_allowed( $this->taxonomy ) ) {
			return false;
		}
		if ( 'edit' === $request['context'] && ! current_user_can( $tax_obj->cap->edit_terms ) ) {
			return new WP_Error( 'rest_forbidden_context', __( 'Sorry, you are not allowed to edit terms in this taxonomy.' ), array( 'status' => rest_authorization_required_code() ) );
		}
		return true;
	}

	/**
	 * Retrieves terms associated with a taxonomy.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_REST_Response|WP_Error Response object on success, or WP_Error object on failure.
	 */
	public function get_items( $request ) {

		// Retrieve the list of registered collection query parameters.
		$registered = $this->get_collection_params();

		/*
		 * This array defines mappings between public API query parameters whose
		 * values are accepted as-passed, and their internal WP_Query parameter
		 * name equivalents (some are the same). Only values which are also
		 * present in $registered will be set.
		 */
		$parameter_mappings = array(
			'exclude'    => 'exclude',
			'include'    => 'include',
			'order'      => 'order',
			'orderby'    => 'orderby',
			'post'       => 'post',
			'hide_empty' => 'hide_empty',
			'per_page'   => 'number',
			'search'     => 'search',
			'slug'       => 'slug',
		);

		$prepared_args = array();

		/*
		 * For each known parameter which is both registered and present in the request,
		 * set the parameter's value on the query $prepared_args.
		 */
		foreach ( $parameter_mappings as $api_param => $wp_param ) {
			if ( isset( $registered[ $api_param ], $request[ $api_param ] ) ) {
				$prepared_args[ $wp_param ] = $request[ $api_param ];
			}
		}

		if ( isset( $registered['offset'] ) && ! empty( $request['offset'] ) ) {
			$prepared_args['offset'] = $request['offset'];
		} else {
			$prepared_args['offset'] = ( $request['page'] - 1 ) * $prepared_args['number'];
		}

		$taxonomy_obj = get_taxonomy( $this->taxonomy );

		if ( $taxonomy_obj->hierarchical && isset( $registered['parent'], $request['parent'] ) ) {
			if ( 0 === $request['parent'] ) {
				// Only query top-level terms.
				$prepared_args['parent'] = 0;
			} else {
				if ( $request['parent'] ) {
					$prepared_args['parent'] = $request['parent'];
				}
			}
		}

		/**
		 * Filters the query arguments before passing them to get_terms().
		 *
		 * The dynamic portion of the hook name, `$this->taxonomy`, refers to the taxonomy slug.
		 *
		 * Enables adding extra arguments or setting defaults for a terms
		 * collection request.
		 *
		 * @since 4.7.0
		 *
		 * @link https://developer.wordpress.org/reference/functions/get_terms/
		 *
		 * @param array           $prepared_args Array of arguments to be
		 *                                       passed to get_terms().
		 * @param WP_REST_Request $request       The current request.
		 */
		$prepared_args = apply_filters( "rest_{$this->taxonomy}_query", $prepared_args, $request );

		if ( ! empty( $prepared_args['post'] )  ) {
			$query_result = wp_get_object_terms( $prepared_args['post'], $this->taxonomy, $prepared_args );

			// Used when calling wp_count_terms() below.
			$prepared_args['object_ids'] = $prepared_args['post'];
		} else {
			$query_result = get_terms( $this->taxonomy, $prepared_args );
		}

		$count_args = $prepared_args;

		unset( $count_args['number'], $count_args['offset'] );

		$total_terms = wp_count_terms( $this->taxonomy, $count_args );

		// wp_count_terms can return a falsy value when the term has no children.
		if ( ! $total_terms ) {
			$total_terms = 0;
		}

		$response = array();

		foreach ( $query_result as $term ) {
			$data = $this->prepare_item_for_response( $term, $request );
			$response[] = $this->prepare_response_for_collection( $data );
		}

		$response = rest_ensure_response( $response );

		// Store pagination values for headers.
		$per_page = (int) $prepared_args['number'];
		$page     = ceil( ( ( (int) $prepared_args['offset'] ) / $per_page ) + 1 );

		$response->header( 'X-WP-Total', (int) $total_terms );

		$max_pages = ceil( $total_terms / $per_page );

		$response->header( 'X-WP-TotalPages', (int) $max_pages );

		$base = add_query_arg( $request->get_query_params(), rest_url( $this->namespace . '/' . $this->rest_base ) );
		if ( $page > 1 ) {
			$prev_page = $page - 1;

			if ( $prev_page > $max_pages ) {
				$prev_page = $max_pages;
			}

			$prev_link = add_query_arg( 'page', $prev_page, $base );
			$response->link_header( 'prev', $prev_link );
		}
		if ( $max_pages > $page ) {
			$next_page = $page + 1;
			$next_link = add_query_arg( 'page', $next_page, $base );

			$response->link_header( 'next', $next_link );
		}

		return $response;
	}

	/**
	 * Get the term, if the ID is valid.
	 *
	 * @since 4.7.2
	 *
	 * @param int $id Supplied ID.
	 * @return WP_Term|WP_Error Term object if ID is valid, WP_Error otherwise.
	 */
	protected function get_term( $id ) {
		$error = new WP_Error( 'rest_term_invalid', __( 'Term does not exist.' ), array( 'status' => 404 ) );

		if ( ! $this->check_is_taxonomy_allowed( $this->taxonomy ) ) {
			return $error;
		}

		if ( (int) $id <= 0 ) {
			return $error;
		}

		$term = get_term( (int) $id, $this->taxonomy );
		if ( empty( $term ) || $term->taxonomy !== $this->taxonomy ) {
			return $error;
		}

		return $term;
	}

	/**
	 * Checks if a request has access to read or edit the specified term.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return bool|WP_Error True if the request has read access for the item, otherwise false or WP_Error object.
	 */
	public function get_item_permissions_check( $request ) {
		$term = $this->get_term( $request['id'] );
		if ( is_wp_error( $term ) ) {
			return $term;
		}

		if ( 'edit' === $request['context'] && ! current_user_can( 'edit_term', $term->term_id ) ) {
			return new WP_Error( 'rest_forbidden_context', __( 'Sorry, you are not allowed to edit this term.' ), array( 'status' => rest_authorization_required_code() ) );
		}
		return true;
	}

	/**
	 * Gets a single term from a taxonomy.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_REST_Response|WP_Error Response object on success, or WP_Error object on failure.
	 */
	public function get_item( $request ) {
		$term = $this->get_term( $request['id'] );

		if ( is_wp_error( $term ) ) {
			return $term;
		}

		$response = $this->prepare_item_for_response( $term, $request );

		return rest_ensure_response( $response );
	}

	/**
	 * Checks if a request has access to create a term.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return bool|WP_Error True if the request has access to create items, false or WP_Error object otherwise.
	 */
	public function create_item_permissions_check( $request ) {

		if ( ! $this->check_is_taxonomy_allowed( $this->taxonomy ) ) {
			return false;
		}

		$taxonomy_obj = get_taxonomy( $this->taxonomy );
		if ( ! current_user_can( $taxonomy_obj->cap->edit_terms ) ) {
			return new WP_Error( 'rest_cannot_create', __( 'Sorry, you are not allowed to create new terms.' ), array( 'status' => rest_authorization_required_code() ) );
		}

		return true;
	}

	/**
	 * Creates a single term in a taxonomy.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_REST_Response|WP_Error Response object on success, or WP_Error object on failure.
	 */
	public function create_item( $request ) {
		if ( isset( $request['parent'] ) ) {
			if ( ! is_taxonomy_hierarchical( $this->taxonomy ) ) {
				return new WP_Error( 'rest_taxonomy_not_hierarchical', __( 'Can not set parent term, taxonomy is not hierarchical.' ), array( 'status' => 400 ) );
			}

			$parent = get_term( (int) $request['parent'], $this->taxonomy );

			if ( ! $parent ) {
				return new WP_Error( 'rest_term_invalid', __( "Parent term doesn't exist." ), array( 'status' => 400 ) );
			}
		}

		$prepared_term = $this->prepare_item_for_database( $request );

		$term = wp_insert_term( wp_slash( $prepared_term->name ), $this->taxonomy, wp_slash( (array) $prepared_term ) );
		if ( is_wp_error( $term ) ) {
			/*
			 * If we're going to inform the client that the term already exists,
			 * give them the identifier for future use.
			 */
			if ( $term_id = $term->get_error_data( 'term_exists' ) ) {
				$existing_term = get_term( $term_id, $this->taxonomy );
				$term->add_data( $existing_term->term_id, 'term_exists' );
			}

			return $term;
		}

		$term = get_term( $term['term_id'], $this->taxonomy );

		/**
		 * Fires after a single term is created or updated via the REST API.
		 *
		 * The dynamic portion of the hook name, `$this->taxonomy`, refers to the taxonomy slug.
		 *
		 * @since 4.7.0
		 *
		 * @param WP_Term         $term     Inserted or updated term object.
		 * @param WP_REST_Request $request  Request object.
		 * @param bool            $creating True when creating a term, false when updating.
		 */
		do_action( "rest_insert_{$this->taxonomy}", $term, $request, true );

		$schema = $this->get_item_schema();
		if ( ! empty( $schema['properties']['meta'] ) && isset( $request['meta'] ) ) {
			$meta_update = $this->meta->update_value( $request['meta'], (int) $request['id'] );

			if ( is_wp_error( $meta_update ) ) {
				return $meta_update;
			}
		}

		$fields_update = $this->update_additional_fields_for_object( $term, $request );

		if ( is_wp_error( $fields_update ) ) {
			return $fields_update;
		}

		$request->set_param( 'context', 'view' );

		$response = $this->prepare_item_for_response( $term, $request );
		$response = rest_ensure_response( $response );

		$response->set_status( 201 );
		$response->header( 'Location', rest_url( $this->namespace . '/' . $this->rest_base . '/' . $term->term_id ) );

		return $response;
	}

	/**
	 * Checks if a request has access to update the specified term.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return bool|WP_Error True if the request has access to update the item, false or WP_Error object otherwise.
	 */
	public function update_item_permissions_check( $request ) {
		$term = $this->get_term( $request['id'] );
		if ( is_wp_error( $term ) ) {
			return $term;
		}

		if ( ! current_user_can( 'edit_term', $term->term_id ) ) {
			return new WP_Error( 'rest_cannot_update', __( 'Sorry, you are not allowed to edit this term.' ), array( 'status' => rest_authorization_required_code() ) );
		}

		return true;
	}

	/**
	 * Updates a single term from a taxonomy.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_REST_Response|WP_Error Response object on success, or WP_Error object on failure.
	 */
	public function update_item( $request ) {
		$term = $this->get_term( $request['id'] );
		if ( is_wp_error( $term ) ) {
			return $term;
		}

		if ( isset( $request['parent'] ) ) {
			if ( ! is_taxonomy_hierarchical( $this->taxonomy ) ) {
				return new WP_Error( 'rest_taxonomy_not_hierarchical', __( 'Can not set parent term, taxonomy is not hierarchical.' ), array( 'status' => 400 ) );
			}

			$parent = get_term( (int) $request['parent'], $this->taxonomy );

			if ( ! $parent ) {
				return new WP_Error( 'rest_term_invalid', __( "Parent term doesn't exist." ), array( 'status' => 400 ) );
			}
		}

		$prepared_term = $this->prepare_item_for_database( $request );

		// Only update the term if we haz something to update.
		if ( ! empty( $prepared_term ) ) {
			$update = wp_update_term( $term->term_id, $term->taxonomy, wp_slash( (array) $prepared_term ) );

			if ( is_wp_error( $update ) ) {
				return $update;
			}
		}

		$term = get_term( $term->term_id, $this->taxonomy );

		/* This action is documented in lib/endpoints/class-wp-rest-terms-controller.php */
		do_action( "rest_insert_{$this->taxonomy}", $term, $request, false );

		$schema = $this->get_item_schema();
		if ( ! empty( $schema['properties']['meta'] ) && isset( $request['meta'] ) ) {
			$meta_update = $this->meta->update_value( $request['meta'], $term->term_id );

			if ( is_wp_error( $meta_update ) ) {
				return $meta_update;
			}
		}

		$fields_update = $this->update_additional_fields_for_object( $term, $request );

		if ( is_wp_error( $fields_update ) ) {
			return $fields_update;
		}

		$request->set_param( 'context', 'view' );

		$response = $this->prepare_item_for_response( $term, $request );

		return rest_ensure_response( $response );
	}

	/**
	 * Checks if a request has access to delete the specified term.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return bool|WP_Error True if the request has access to delete the item, otherwise false or WP_Error object.
	 */
	public function delete_item_permissions_check( $request ) {
		$term = $this->get_term( $request['id'] );
		if ( is_wp_error( $term ) ) {
			return $term;
		}

		if ( ! current_user_can( 'delete_term', $term->term_id ) ) {
			return new WP_Error( 'rest_cannot_delete', __( 'Sorry, you are not allowed to delete this term.' ), array( 'status' => rest_authorization_required_code() ) );
		}

		return true;
	}

	/**
	 * Deletes a single term from a taxonomy.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Full details about the request.
	 * @return WP_REST_Response|WP_Error Response object on success, or WP_Error object on failure.
	 */
	public function delete_item( $request ) {
		$term = $this->get_term( $request['id'] );
		if ( is_wp_error( $term ) ) {
			return $term;
		}

		$force = isset( $request['force'] ) ? (bool) $request['force'] : false;

		// We don't support trashing for terms.
		if ( ! $force ) {
			return new WP_Error( 'rest_trash_not_supported', __( 'Terms do not support trashing. Set force=true to delete.' ), array( 'status' => 501 ) );
		}

		$request->set_param( 'context', 'view' );

		$previous = $this->prepare_item_for_response( $term, $request );

		$retval = wp_delete_term( $term->term_id, $term->taxonomy );

		if ( ! $retval ) {
			return new WP_Error( 'rest_cannot_delete', __( 'The term cannot be deleted.' ), array( 'status' => 500 ) );
		}

		$response = new WP_REST_Response();
		$response->set_data( array( 'deleted' => true, 'previous' => $previous->get_data() ) );

		/**
		 * Fires after a single term is deleted via the REST API.
		 *
		 * The dynamic portion of the hook name, `$this->taxonomy`, refers to the taxonomy slug.
		 *
		 * @since 4.7.0
		 *
		 * @param WP_Term          $term     The deleted term.
		 * @param WP_REST_Response $response The response data.
		 * @param WP_REST_Request  $request  The request sent to the API.
		 */
		do_action( "rest_delete_{$this->taxonomy}", $term, $response, $request );

		return $response;
	}

	/**
	 * Prepares a single term for create or update.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param WP_REST_Request $request Request object.
	 * @return object $prepared_term Term object.
	 */
	public function prepare_item_for_database( $request ) {
		$prepared_term = new stdClass;

		$schema = $this->get_item_schema();
		if ( isset( $request['name'] ) && ! empty( $schema['properties']['name'] ) ) {
			$prepared_term->name = $request['name'];
		}

		if ( isset( $request['slug'] ) && ! empty( $schema['properties']['slug'] ) ) {
			$prepared_term->slug = $request['slug'];
		}

		if ( isset( $request['taxonomy'] ) && ! empty( $schema['properties']['taxonomy'] ) ) {
			$prepared_term->taxonomy = $request['taxonomy'];
		}

		if ( isset( $request['description'] ) && ! empty( $schema['properties']['description'] ) ) {
			$prepared_term->description = $request['description'];
		}

		if ( isset( $request['parent'] ) && ! empty( $schema['properties']['parent'] ) ) {
			$parent_term_id = 0;
			$parent_term    = get_term( (int) $request['parent'], $this->taxonomy );

			if ( $parent_term ) {
				$parent_term_id = $parent_term->term_id;
			}

			$prepared_term->parent = $parent_term_id;
		}

		/**
		 * Filters term data before inserting term via the REST API.
		 *
		 * The dynamic portion of the hook name, `$this->taxonomy`, refers to the taxonomy slug.
		 *
		 * @since 4.7.0
		 *
		 * @param object          $prepared_term Term object.
		 * @param WP_REST_Request $request       Request object.
		 */
		return apply_filters( "rest_pre_insert_{$this->taxonomy}", $prepared_term, $request );
	}

	/**
	 * Prepares a single term output for response.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @param obj             $item    Term object.
	 * @param WP_REST_Request $request Request object.
	 * @return WP_REST_Response $response Response object.
	 */
	public function prepare_item_for_response( $item, $request ) {

		$schema = $this->get_item_schema();
		$data   = array();

		if ( ! empty( $schema['properties']['id'] ) ) {
			$data['id'] = (int) $item->term_id;
		}

		if ( ! empty( $schema['properties']['count'] ) ) {
			$data['count'] = (int) $item->count;
		}

		if ( ! empty( $schema['properties']['description'] ) ) {
			$data['description'] = $item->description;
		}

		if ( ! empty( $schema['properties']['link'] ) ) {
			$data['link'] = get_term_link( $item );
		}

		if ( ! empty( $schema['properties']['name'] ) ) {
			$data['name'] = $item->name;
		}

		if ( ! empty( $schema['properties']['slug'] ) ) {
			$data['slug'] = $item->slug;
		}

		if ( ! empty( $schema['properties']['taxonomy'] ) ) {
			$data['taxonomy'] = $item->taxonomy;
		}

		if ( ! empty( $schema['properties']['parent'] ) ) {
			$data['parent'] = (int) $item->parent;
		}

		if ( ! empty( $schema['properties']['meta'] ) ) {
			$data['meta'] = $this->meta->get_value( $item->term_id, $request );
		}

		$context = ! empty( $request['context'] ) ? $request['context'] : 'view';
		$data    = $this->add_additional_fields_to_object( $data, $request );
		$data    = $this->filter_response_by_context( $data, $context );

		$response = rest_ensure_response( $data );

		$response->add_links( $this->prepare_links( $item ) );

		/**
		 * Filters a term item returned from the API.
		 *
		 * The dynamic portion of the hook name, `$this->taxonomy`, refers to the taxonomy slug.
		 *
		 * Allows modification of the term data right before it is returned.
		 *
		 * @since 4.7.0
		 *
		 * @param WP_REST_Response  $response  The response object.
		 * @param object            $item      The original term object.
		 * @param WP_REST_Request   $request   Request used to generate the response.
		 */
		return apply_filters( "rest_prepare_{$this->taxonomy}", $response, $item, $request );
	}

	/**
	 * Prepares links for the request.
	 *
	 * @since 4.7.0
	 * @access protected
	 *
	 * @param object $term Term object.
	 * @return array Links for the given term.
	 */
	protected function prepare_links( $term ) {
		$base = $this->namespace . '/' . $this->rest_base;
		$links = array(
			'self'       => array(
				'href' => rest_url( trailingslashit( $base ) . $term->term_id ),
			),
			'collection' => array(
				'href' => rest_url( $base ),
			),
			'about'      => array(
				'href' => rest_url( sprintf( 'wp/v2/taxonomies/%s', $this->taxonomy ) ),
			),
		);

		if ( $term->parent ) {
			$parent_term = get_term( (int) $term->parent, $term->taxonomy );

			if ( $parent_term ) {
				$links['up'] = array(
					'href'       => rest_url( trailingslashit( $base ) . $parent_term->term_id ),
					'embeddable' => true,
				);
			}
		}

		$taxonomy_obj = get_taxonomy( $term->taxonomy );

		if ( empty( $taxonomy_obj->object_type ) ) {
			return $links;
		}

		$post_type_links = array();

		foreach ( $taxonomy_obj->object_type as $type ) {
			$post_type_object = get_post_type_object( $type );

			if ( empty( $post_type_object->show_in_rest ) ) {
				continue;
			}

			$rest_base = ! empty( $post_type_object->rest_base ) ? $post_type_object->rest_base : $post_type_object->name;
			$post_type_links[] = array(
				'href' => add_query_arg( $this->rest_base, $term->term_id, rest_url( sprintf( 'wp/v2/%s', $rest_base ) ) ),
			);
		}

		if ( ! empty( $post_type_links ) ) {
			$links['https://api.w.org/post_type'] = $post_type_links;
		}

		return $links;
	}

	/**
	 * Retrieves the term's schema, conforming to JSON Schema.
	 *
	 * @since 4.7.0
	 * @access public
	 *
	 * @return array Item schema data.
	 */
	public function get_item_schema() {
		$schema = array(
			'$schema' @ 8?> 'htTu/bsgns�lu}a.>vO/7c`lma#7l.		%`itea�  D$ "=> ��o[p��iw�-= �this~$ax7noKy$2 �ttg'  $Tpmc%.|30O$oiI,
-	%�m�eG  0(x` }�,oncg�|'��	�9gprovG�TAes. 5~ ��a�`	M	'hd'"0! "  p( ->(ar2ay�		�.d�wappi{�$�=>'_{H 'nIuu5 ) knt�fier dov�qIe e�e&c$�-
	#	�'typtE4 h �h�!a?�h�+n�ouz',
	9�'c/^T%x4'$   b��> qbzey!h&vIUw�`%MB�d'<$'�diP�0(�
�1		y7rEidofl}��   $9�"}uE.+		(� �	�Co�n47� 0b ( �>"arRax,H		'd%qc�iv2inn 0=.�l�( /Nuojuz Mf puc,yr�$l �o2p3 �mR `�d turm.'�#l			I	'w}r$' &  %!$ �=> �ilxegAzO$*	I	('c/lv|xt5   *� =. !rS!m)!Iew', 'e�(d' !�A	7fe@dk,lI',    -> v3eql
	!h@		!'de3Cvh�tioo'05�	A�pAq(zI#deccrix�)ol'& 56 WO(*'HtIL dgsCRi1�l���of2�ha(qEr.n?$*nK�	K�t�ve%"(,,  $b =>`svrcM�.
			�con��z�7   !4%5>"a2zey*�'vi%p'(�wc i|�@,�		`.	�lHi'�$  8� #5> crrqY�	!	c$ercr)ptIMf' 096 v(`URL gv$ h� 4e }>),		'v�pe'���*�&� ��'s �HowG�
I	�'gg�m&|'� 0�   => uwm%(*	I'j�Jtd~t%�" <$"=.  ray* $g{ewod�'e�fmb'~ %�d)t')),
-	'zeD_�l}/�  `0=? u{xe
	!,,�		g,imE�!Z`h a  �0`2rayH�		I	�'�gsari0tiwN&0 4��O[� 'X�m|�$}�\%�no{��b� tE�i%�!k,ɩ'tyxu'00``a b,1=~07C4vinOg%	)�.�mnteX�! $$ 0> a�rgy(P'viEW&`�eefel'( ��DA�7 �
	I		/arVmQ�y/n�/0$}? arra(J				%&�e.i4yZe^jc��bae�`=~"'s!fotkzd_tepT?�Ywl�&,j)�			.2		Xsequ	b%$�"��0(o`tr5E,J�)-)l
�Y�'3�u'% !``0(  =~`a�zai,
			)-D5rcvi�ti��'!`�@__("eQn0)l1hanq(��i# mde�tif`ev#DoRdth�$team�gn�ww] t8i| tyua�/ !(.		)9	�4yq' #"a`$p#!]50'stri.g�,�'conte�t%!!2 0 ?>$!2rA� b#vqqw�0'dd"gt' 'e@q|' 	,���I	'��'^_ptikNs( }? ArrAy9>		)�	scnkfizg�ja|l�dcK'!=6�ev2!{8@&T(is. &{An�iz�_zDu� ).
		�	I),	I		�,�-	gtaxobouy&��� =6 axbcY�
	)K	�fms'rip�i/l/  ?? _ $cUy0��a5�bi�qu�o~ dov DhE$�c2}.� )B	�)m'tytef`"!" 0� 
<. 7srr�nF/�	�	%unu�e$$��  ( �>`cr���_oeuQ(%euU�}a�kGo�i%s(� 	/�I	)c�ontg|�/"0   �=6�avr!m. '�ie7$ �el`ed', �Ediu' +,	PcriadoJlyo��,)�=> trw�,�	�)J�
	),*	!=
�	$4axo|n�9 �1gatua0ojn-�(!,P�is-6tc�oj?m9 );J	qf (06xax/om�>8kerAbjii/ql0- {@)$sch%m![%rrgqe�to�S7U['1er�nt/] �0AVrAy C�	I	%��sCRapxkkF�($q[U(�Tdt 2aq�.$$ua�$ID.g`#.	)typeg $� �# � m:�'Hb�eG%�',
-	)'f�ftt& � ! `=| a�Rq�`%v���'> 'x&IT7")	);
iy

)	$ssheMao`3otdrti�s7][.meTK'] - d4jiq/�-et%/6c�|Wf�Hd^rch'�c9);�-1Vaturn0 tI�Qm>�Lb_y&dyt!ol�h_fiEnd3�ac�E�{( %;jleMi a+	y
�*  n�RepraEreq�~hm Ce%R��pArm)s�fos �o|leiviOnCIc.I`
 HsiNae$%*�.0	 *@@�cbess `u`lls�!��J	 +$@we|uro�!z�a�$[+mDec2Aoo(par�k�uer�.2%:�
	qUrdi f}�s�}on guf�s/m�e�tif~_piV�ms��`ɋdawmry^xarsesd=0pa�1nt:vnmtW�k.�e�0)o/_|{�a|�(!+	$tax�n/my � o%puax�n/mi(2$thys+>p`xkn�ey );
,aozy�p`6�m;�'c��tt|t%\�&�agaql�'] � '2Aew'�
R	A$quer��ph"cm�Qecdm|�c}`0ir0ah(
		7d1igrm"tw�j5 (`2�  =>$?^) 7fa�ru�Pewtl�!wev�exB~-Ee30wegmVH� 	Dc.' �.
	&5yq�'�      d 0pd�"6 'arriy'��	I'ate�s'  �$� ��    (=.0AD�aY(J)	T9pu& "4 $$  �!> 'il�%Gdz
!�		,
	�eFauAt' ! 0  ` !"=�(�pra}9�bA)8
I$1U5ba_tArqms;a�cl�dq']}"azb#y,
	'pa2c0itt�On#      9|$^_( �Hil�v �ewtl0 set {g�cPd"c�Ic$)t3�'*!�
�)	'�1`g%& (  !2 0� 2 (=>�%kzgs'�		icktemS� �A � #�   0 ;6!drray�*	�/4yhg'  :�$�  � =?"'inp�ferg#IY,
		3dab�uld7`$  0h   �<> axra9	-.J	;
J		k�:�p%(&tAxonOly).�hbasahhcal ) {���uMp}_paS�ms�offq�d/}�9 arvaq(
Y#!u�QriPtjobo`$ b  =z _]( 'O�~{Gt 4Hg0rU�e\tset pi q!r0ecifkr!�Umbgb"nF"i�goc� )=	g�Yu%?1   0       !<6 ?`npEgr�,	�k
		�*	)1quepq]z�r�-r['#rdm2'M�9BeraY(
		�'meSc�eptaod��$  0�!?� U_( Oad�p 3�rt a|Rrhjmte(irceNdin�"o2 ue��anEllC*3(9
		�'Tyth'd 1 �h  "�@#
(?�#'st2!�u&<J		�e%g�q|p/ `( (0   `?>*5`sc.	�I7�p��     !` !  0,=�0avrA19��	'arc/B	)#�e�c,
	)
�-�
�	$9uesyOqaz�osYg�"derby'y0� irb@�(		dg3g�!�t%nn'!� c0` 9= I[!'soru g/�l�c0mg~"`x TerM���tPq`�te*� !(
)	�PypE#(`( 20  !b"$0 = Vpvifgo�
	 e&Eslt�p`$   @ 0"=~�'nam�'$�Ygegum' p�"   � 1!$  =>`!r2`{(�	i'ed�,J		ifCd�ee'S	i'n!m�',�	'{,uf'-
	I�'<erl^g�o�`',�	�I<escrit�nc,.			'sgt|t'-
	��),J��)�
	!$1U%3{_qh~AeS'hi$�]e-pty'�09&!�2�{(
	�%`eR�sipuio�'((   f !_((3WhMdhev tn��de �{3 /� !C�mw�ul �o�An} pKcd��'),[et|�e' �$   0  �( j,�60&goolesf'L 		'degeul' ( (     $(=7 fpn3E,J		)	i"() $|aXoommy%6(�gqip;�mss, ,0	I) }eE2qWvavio�Z/pareNt�Y"= Array(
		+	/`e#Ra��ion'  �   �5>"{( Lih�t$vesql< rwt#tO dg2m2 �cs)g&gg!qo"` spe#i&k� purdjt/�),		�ey8E   @`$�$! d b>'h�veee�gl
	�	);
	}
(9$q�mpy>0I2�m�X�0Ost�]�= a�sqq8*#g �sc�h�\hgN'&"�   `�>�WV8b'LAyit$j�qu�� qet(va�|or-r�bssig@�tg`a!rtuc@fic �g3f/" )�
M	)e�p�e �$ "  � 0  *<�!'a�ta'�e(*I	%eat�U,�#(` 0    �%&�z$.=hl=
	)9>
Y5q�a��_0a2a-s[EsLue�Y(= asbay�J		�'�awk�ih�eon� a (P  6 O("'L#nHU�z%stLt set �o teryspwitj$e>sPmk9vyC`zl5g�U i,
			�4qx�+ �q  0(b 0�% "=�"'cbpeqo��A�mdu�3(8    ( 0 )&`>"if2qy(
�		�'�q�e'$���   �p 9z(�svrkGgg
	�)$�M��
/*�
  : Nyhtgr �llac|ao� pasy�ebers"��R t`u!vdR9p�cOl�rn|l�q.4(.I *�^jm `in`'+#"5act"Of phu �k�pez! $l`a{->t�yonmy �raf#v� |k0t`�`vax�jolI"		 *dsnug �3 tiekooU2ol�mr.
  *�	 * TnIs filF)r`z�uak|Erc$p�} Solle�qhef4��rq,u�a�, jU4vER oo|&}qx"�ae	(*�c��dag4iof paj`iexqr T� qn`�&4u ngl!vR]dco_Qu13� �ar�Le��~��$u�c Xie			 * ireSt�{Mtlhs->��p.~om�}quer9@!vi�ver"To s� SP�U%e_y5erm P`Rh�%weRsn�	 3	 j hSy�ce��.5>1	1{h
 +0`paSqi$@rvax  $  $ �qq%*X_P�eMs@JRON S#0dE$od�e�|�d Comlek`m� PAwi}eter��	 * @]a�a},�T�XQpgfomY $teY~�o,`   (Wepnno-y�.D*m#�~�Y�*/�))qw�ebj(hPT,y_bklT'Rq*�"RdS`Skd�(kc-rixgnk,y}�ahnl%c5I�n�p!`�M�"$ ,�wer;��!{eIc, dvexOoom{$)?
	}�)3*j ��"*C�{kS�vx@0 t�u raxm^ooݣis(�!l)dnB� h	�* Xsi�cE`/#�	b*b`CcgSu trlpdcte`	 *!:`@pbra2uuR9.f!|a8Oo,oy(Tahznkm9 to��)dBk
8!* Pret}:n0b�-l�W�'t|er!�(d uEqonm�8is alln�Ed dos ZM�D nan�amu.�.
 *��	pso0da5a f�nctin��g�ec[_�s�T1�mwg�y_al,o��8%duixcN}� *!Z�$t`y�n/m�oj  ?`�Et_f�zN�mM  TayOnomy4)
	H�v`) %uAx/Iolx_ocj &&�!!%m�f|(8t`hoNk-y]f"j��[ho7Oin�zrv )`� {JI�r}tu2�)truE[
�}M	pd}��N1filse;
	u
u