<?php
/**
 * Core Metadata API
 *
 * Functions for retrieving and manipulating metadata of various WordPress object types. Metadata
 * for an object is a represented by a simple key-value pair. Objects may contain multiple
 * metadata entries that share the same key and differ only in their value.
 *
 * @package WordPress
 * @subpackage Meta
 */

/**
 * Add metadata for the specified object.
 *
 * @since 2.9.0
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 *
 * @param string $meta_type  Type of object metadata is for (e.g., comment, post, or user)
 * @param int    $object_id  ID of the object metadata is for
 * @param string $meta_key   Metadata key
 * @param mixed  $meta_value Metadata value. Must be serializable if non-scalar.
 * @param bool   $unique     Optional, default is false.
 *                           Whether the specified metadata key should be unique for the object.
 *                           If true, and the object already has a value for the specified metadata key,
 *                           no change will be made.
 * @return int|false The meta ID on success, false on failure.
 */
function add_metadata($meta_type, $object_id, $meta_key, $meta_value, $unique = false) {
	global $wpdb;

	if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) ) {
		return false;
	}

	$object_id = absint( $object_id );
	if ( ! $object_id ) {
		return false;
	}

	$table = _get_meta_table( $meta_type );
	if ( ! $table ) {
		return false;
	}

	$column = sanitize_key($meta_type . '_id');

	// expected_slashed ($meta_key)
	$meta_key = wp_unslash($meta_key);
	$meta_value = wp_unslash($meta_value);
	$meta_value = sanitize_meta( $meta_key, $meta_value, $meta_type );

	/**
	 * Filters whether to add metadata of a specific type.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user). Returning a non-null value
	 * will effectively short-circuit the function.
	 *
	 * @since 3.1.0
	 *
	 * @param null|bool $check      Whether to allow adding metadata for the given type.
	 * @param int       $object_id  Object ID.
	 * @param string    $meta_key   Meta key.
	 * @param mixed     $meta_value Meta value. Must be serializable if non-scalar.
	 * @param bool      $unique     Whether the specified meta key should be unique
	 *                              for the object. Optional. Default false.
	 */
	$check = apply_filters( "add_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $unique );
	if ( null !== $check )
		return $check;

	if ( $unique && $wpdb->get_var( $wpdb->prepare(
		"SELECT COUNT(*) FROM $table WHERE meta_key = %s AND $column = %d",
		$meta_key, $object_id ) ) )
		return false;

	$_meta_value = $meta_value;
	$meta_value = maybe_serialize( $meta_value );

	/**
	 * Fires immediately before meta of a specific type is added.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user).
	 *
	 * @since 3.1.0
	 *
	 * @param int    $object_id  Object ID.
	 * @param string $meta_key   Meta key.
	 * @param mixed  $meta_value Meta value.
	 */
	do_action( "add_{$meta_type}_meta", $object_id, $meta_key, $_meta_value );

	$result = $wpdb->insert( $table, array(
		$column => $object_id,
		'meta_key' => $meta_key,
		'meta_value' => $meta_value
	) );

	if ( ! $result )
		return false;

	$mid = (int) $wpdb->insert_id;

	wp_cache_delete($object_id, $meta_type . '_meta');

	/**
	 * Fires immediately after meta of a specific type is added.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user).
	 *
	 * @since 2.9.0
	 *
	 * @param int    $mid        The meta ID after successful update.
	 * @param int    $object_id  Object ID.
	 * @param string $meta_key   Meta key.
	 * @param mixed  $meta_value Meta value.
	 */
	do_action( "added_{$meta_type}_meta", $mid, $object_id, $meta_key, $_meta_value );

	return $mid;
}

/**
 * Update metadata for the specified object. If no value already exists for the specified object
 * ID and metadata key, the metadata will be added.
 *
 * @since 2.9.0
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 *
 * @param string $meta_type  Type of object metadata is for (e.g., comment, post, or user)
 * @param int    $object_id  ID of the object metadata is for
 * @param string $meta_key   Metadata key
 * @param mixed  $meta_value Metadata value. Must be serializable if non-scalar.
 * @param mixed  $prev_value Optional. If specified, only update existing metadata entries with
 * 		                     the specified value. Otherwise, update all entries.
 * @return int|bool Meta ID if the key didn't exist, true on successful update, false on failure.
 */
function update_metadata($meta_type, $object_id, $meta_key, $meta_value, $prev_value = '') {
	global $wpdb;

	if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) ) {
		return false;
	}

	$object_id = absint( $object_id );
	if ( ! $object_id ) {
		return false;
	}

	$table = _get_meta_table( $meta_type );
	if ( ! $table ) {
		return false;
	}

	$column = sanitize_key($meta_type . '_id');
	$id_column = 'user' == $meta_type ? 'umeta_id' : 'meta_id';

	// expected_slashed ($meta_key)
	$raw_meta_key = $meta_key;
	$meta_key = wp_unslash($meta_key);
	$passed_value = $meta_value;
	$meta_value = wp_unslash($meta_value);
	$meta_value = sanitize_meta( $meta_key, $meta_value, $meta_type );

	/**
	 * Filters whether to update metadata of a specific type.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user). Returning a non-null value
	 * will effectively short-circuit the function.
	 *
	 * @since 3.1.0
	 *
	 * @param null|bool $check      Whether to allow updating metadata for the given type.
	 * @param int       $object_id  Object ID.
	 * @param string    $meta_key   Meta key.
	 * @param mixed     $meta_value Meta value. Must be serializable if non-scalar.
	 * @param mixed     $prev_value Optional. If specified, only update existing
	 *                              metadata entries with the specified value.
	 *                              Otherwise, update all entries.
	 */
	$check = apply_filters( "update_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $prev_value );
	if ( null !== $check )
		return (bool) $check;

	// Compare existing value to new value if no prev value given and the key exists only once.
	if ( empty($prev_value) ) {
		$old_value = get_metadata($meta_type, $object_id, $meta_key);
		if ( count($old_value) == 1 ) {
			if ( $old_value[0] === $meta_value )
				return false;
		}
	}

	$meta_ids = $wpdb->get_col( $wpdb->prepare( "SELECT $id_column FROM $table WHERE meta_key = %s AND $column = %d", $meta_key, $object_id ) );
	if ( empty( $meta_ids ) ) {
		return add_metadata( $meta_type, $object_id, $raw_meta_key, $passed_value );
	}

	$_meta_value = $meta_value;
	$meta_value = maybe_serialize( $meta_value );

	$data  = compact( 'meta_value' );
	$where = array( $column => $object_id, 'meta_key' => $meta_key );

	if ( !empty( $prev_value ) ) {
		$prev_value = maybe_serialize($prev_value);
		$where['meta_value'] = $prev_value;
	}

	foreach ( $meta_ids as $meta_id ) {
		/**
		 * Fires immediately before updating metadata of a specific type.
		 *
		 * The dynamic portion of the hook, `$meta_type`, refers to the meta
		 * object type (comment, post, or user).
		 *
		 * @since 2.9.0
		 *
		 * @param int    $meta_id    ID of the metadata entry to update.
		 * @param int    $object_id  Object ID.
		 * @param string $meta_key   Meta key.
		 * @param mixed  $meta_value Meta value.
		 */
		do_action( "update_{$meta_type}_meta", $meta_id, $object_id, $meta_key, $_meta_value );

		if ( 'post' == $meta_type ) {
			/**
			 * Fires immediately before updating a post's metadata.
			 *
			 * @since 2.9.0
			 *
			 * @param int    $meta_id    ID of metadata entry to update.
			 * @param int    $object_id  Object ID.
			 * @param string $meta_key   Meta key.
			 * @param mixed  $meta_value Meta value.
			 */
			do_action( 'update_postmeta', $meta_id, $object_id, $meta_key, $meta_value );
		}
	}

	$result = $wpdb->update( $table, $data, $where );
	if ( ! $result )
		return false;

	wp_cache_delete($object_id, $meta_type . '_meta');

	foreach ( $meta_ids as $meta_id ) {
		/**
		 * Fires immediately after updating metadata of a specific type.
		 *
		 * The dynamic portion of the hook, `$meta_type`, refers to the meta
		 * object type (comment, post, or user).
		 *
		 * @since 2.9.0
		 *
		 * @param int    $meta_id    ID of updated metadata entry.
		 * @param int    $object_id  Object ID.
		 * @param string $meta_key   Meta key.
		 * @param mixed  $meta_value Meta value.
		 */
		do_action( "updated_{$meta_type}_meta", $meta_id, $object_id, $meta_key, $_meta_value );

		if ( 'post' == $meta_type ) {
			/**
			 * Fires immediately after updating a post's metadata.
			 *
			 * @since 2.9.0
			 *
			 * @param int    $meta_id    ID of updated metadata entry.
			 * @param int    $object_id  Object ID.
			 * @param string $meta_key   Meta key.
			 * @param mixed  $meta_value Meta value.
			 */
			do_action( 'updated_postmeta', $meta_id, $object_id, $meta_key, $meta_value );
		}
	}

	return true;
}

/**
 * Delete metadata for the specified object.
 *
 * @since 2.9.0
 *
 * @global wpdb $wpdb WordPress database abstraction object.
 *
 * @param string $meta_type  Type of object metadata is for (e.g., comment, post, or user)
 * @param int    $object_id  ID of the object metadata is for
 * @param string $meta_key   Metadata key
 * @param mixed  $meta_value Optional. Metadata value. Must be serializable if non-scalar. If specified, only delete
 *                           metadata entries with this value. Otherwise, delete all entries with the specified meta_key.
 *                           Pass `null, `false`, or an empty string to skip this check. (For backward compatibility,
 *                           it is not possible to pass an empty string to delete those entries with an empty string
 *                           for a value.)
 * @param bool   $delete_all Optional, default is false. If true, delete matching metadata entries for all objects,
 *                           ignoring the specified object_id. Otherwise, only delete matching metadata entries for
 *                           the specified object_id.
 * @return bool True on successful delete, false on failure.
 */
function delete_metadata($meta_type, $object_id, $meta_key, $meta_value = '', $delete_all = false) {
	global $wpdb;

	if ( ! $meta_type || ! $meta_key || ! is_numeric( $object_id ) && ! $delete_all ) {
		return false;
	}

	$object_id = absint( $object_id );
	if ( ! $object_id && ! $delete_all ) {
		return false;
	}

	$table = _get_meta_table( $meta_type );
	if ( ! $table ) {
		return false;
	}

	$type_column = sanitize_key($meta_type . '_id');
	$id_column = 'user' == $meta_type ? 'umeta_id' : 'meta_id';
	// expected_slashed ($meta_key)
	$meta_key = wp_unslash($meta_key);
	$meta_value = wp_unslash($meta_value);

	/**
	 * Filters whether to delete metadata of a specific type.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user). Returning a non-null value
	 * will effectively short-circuit the function.
	 *
	 * @since 3.1.0
	 *
	 * @param null|bool $delete     Whether to allow metadata deletion of the given type.
	 * @param int       $object_id  Object ID.
	 * @param string    $meta_key   Meta key.
	 * @param mixed     $meta_value Meta value. Must be serializable if non-scalar.
	 * @param bool      $delete_all Whether to delete the matching metadata entries
	 *                              for all objects, ignoring the specified $object_id.
	 *                              Default false.
	 */
	$check = apply_filters( "delete_{$meta_type}_metadata", null, $object_id, $meta_key, $meta_value, $delete_all );
	if ( null !== $check )
		return (bool) $check;

	$_meta_value = $meta_value;
	$meta_value = maybe_serialize( $meta_value );

	$query = $wpdb->prepare( "SELECT $id_column FROM $table WHERE meta_key = %s", $meta_key );

	if ( !$delete_all )
		$query .= $wpdb->prepare(" AND $type_column = %d", $object_id );

	if ( '' !== $meta_value && null !== $meta_value && false !== $meta_value )
		$query .= $wpdb->prepare(" AND meta_value = %s", $meta_value );

	$meta_ids = $wpdb->get_col( $query );
	if ( !count( $meta_ids ) )
		return false;

	if ( $delete_all ) {
		if ( '' !== $meta_value && null !== $meta_value && false !== $meta_value ) {
			$object_ids = $wpdb->get_col( $wpdb->prepare( "SELECT $type_column FROM $table WHERE meta_key = %s AND meta_value = %s", $meta_key, $meta_value ) );
		} else {
			$object_ids = $wpdb->get_col( $wpdb->prepare( "SELECT $type_column FROM $table WHERE meta_key = %s", $meta_key ) );
		}
	}

	/**
	 * Fires immediately before deleting metadata of a specific type.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user).
	 *
	 * @since 3.1.0
	 *
	 * @param array  $meta_ids   An array of metadata entry IDs to delete.
	 * @param int    $object_id  Object ID.
	 * @param string $meta_key   Meta key.
	 * @param mixed  $meta_value Meta value.
	 */
	do_action( "delete_{$meta_type}_meta", $meta_ids, $object_id, $meta_key, $_meta_value );

	// Old-style action.
	if ( 'post' == $meta_type ) {
		/**
		 * Fires immediately before deleting metadata for a post.
		 *
		 * @since 2.9.0
		 *
		 * @param array $meta_ids An array of post metadata entry IDs to delete.
		 */
		do_action( 'delete_postmeta', $meta_ids );
	}

	$query = "DELETE FROM $table WHERE $id_column IN( " . implode( ',', $meta_ids ) . " )";

	$count = $wpdb->query($query);

	if ( !$count )
		return false;

	if ( $delete_all ) {
		foreach ( (array) $object_ids as $o_id ) {
			wp_cache_delete($o_id, $meta_type . '_meta');
		}
	} else {
		wp_cache_delete($object_id, $meta_type . '_meta');
	}

	/**
	 * Fires immediately after deleting metadata of a specific type.
	 *
	 * The dynamic portion of the hook name, `$meta_type`, refers to the meta
	 * object type (comment, post, or user).
	 *
	 * @since 2.9.0
	 *
	 * @param array  $meta_ids   An array of deleted metadata entry IDs.
	 * @param int    $object_id  Object ID.
	 * @param string $meta_key   Meta key.
	 * @param mixed  $meta_value Meta value.
	 */
	do_action( "deleted_{$meta_type}_meta", $meta_ids, $object_id, $meta_key, $_meta_value );

	// Old-style action.
	if ( 'post' == $meta_type ) {
		/**
		 * Fires immediately after deleting metadata for a post.
		 *
		 * @since 2.9.0
		 *
		 * @param array $meta_ids An array of deleted post metadata entry IDs.
		 */
		do_action( 'deleted_postmeta', $meta_ids );
	}

	return true;
}

/**
 * Retrieve metadata for the specified object.
 *
 * @since 2.9.0
 *
 * @param string $meta_type Type of object metadata is for (e.g., comment, post, or user)
 * @param int    $object_id ID of the object metadata is for
 * @param string $meta_key  Optional. Metadata key. If not specified, retrieve all metadata for
 * 		                    the specified object.
 * @param bool   $single    Optional, default is false.
 *                          If true, return only the first value of the specified meta_key.
 *                          This parameter has no effect if meta_key is not specified.
 * @return mixed Single metadata value, or array of values
 */
function get_metadata($meta_type, $object_id, $meta_key = '', $single = false) {
	if ( ! $meta_type || ! is_numeric( $object_id ) ) {
		return false;
	}

	$object_id = absint( $object_id );
	if ( ! $object_id ) {
		return false;
	}

	/**
	 * Filters whether to retrieve metadata of a specific type.
	 *
	 * The dynamic portion of the hook, `$meta_type`, refers to the meta
	 * object type (comment, post, or user). Returning a non-null value
	 * will effectively short-circuit the function.
	 *
	 * @since 3.1.0
	 *
	 * @param null|array|string $value     The value get_metadata() should return - a single metadata value,
	 *                                     or an array of values.
	 * @param int               $object_id Object ID.
	 * @param string            $meta_key  Meta key.
	 * @param bool              $single    Whether to return only the first value of the specified $met�{ly.
*-AahgcC 5 av`l[wfintdvs( 1EotY�mUra|vyrU}-dt1d�t�!�hnum�,$$d�jas~]i ,(%mbahex$ !Sangld 9;:	if ( .uMl !5= �s�eck )"{B		ib 0($s�Lg$u �*ywWqcraQ�%!e/ec�0�#=
KpUttrN"$s`!f{[ ];
�	%lsd�			se�ebn&+he
k1�q

	$]muaWcihfa=#w`_c�ghu]gE`(�mbnoct)d-"$O0i~|�tD$? '^let�#;
	av`( !$mwtc_c#S(�`) s(	$me4!_c�Blm2 t`d�te[Mg�_KaGx�8 $me�a_|�`լ �p{qy("%object��` ) )J�metq_cakHa0�"met�?#y�jgY$/BjdfTid];
} 
	ab  � ,yeta]kaY0�`�	Meu}vn` mgt`Oc1c�e;��J!f *�i�SDDl$oAt�G`acj�S$gata^keW})(+0o�		i� H dsy�gld$)
	Iretu>f yc;fe�unc%riayiz�($mgD`_oacie[$a�A�keY_C0� (9
I	en�e�	Irewrj#ar:)yOmut 7}i9Be_un;lzi*L�zu�& $odFc_"hchmSdm�ui_kg}�)	}

	(w!jfVijele-
�[rm�ern�'?E.se
	p�ue2n�`bvcux){$|8/
�� *0DgpE�LilU`if Ame�� oes8h;(s�v&enrqh�hVidmB�ecl
x�
*�sibrg`3.S.0
`*
 �0a2y�!m strmN/(imu!^L{q%!Tyqe$nfh/bhecd Mdt!ea~a a#�F�2 (Ed'.,(comme~d$ �){t���R�ds�v)� * B�p�qm 9f| �!`$o�k�c~id ID`/f"Thghgbj#cp mGt�dc0q � for
 * !6p`m!2trhnà�Me�!Whgz  �E|)le~a oap. *@REtuvn�#ool TpUu ob" hd ku�(My se�l �bls� i nnt. (O�<nk|�o�lgtatata_qxIq�� �M4�[4�pu�@$�"jcb|_�d< �o%�e�kgy�! {
i& ( ��dmeta^y8p�|| k isS*u}fri" (&O"zEcT~Id )$+ �
		ret}rn�alS$9Iu	��FB�b~_Id`= afsi}|( $o"je�|^mdh)3
Ia4$a #nBj�s([id ))[��bet]Vn F!,se;�}�*�Fjat�mq �)lddr iS0dfuMe\�  in"70�a�lubqs-dva>�bp(:Qc�e#a0< cpply_fi,xers.p#oUt_k meT!tyP�]]��tamiTq3, .unn$$<mbje#t���,�'me|iUkfxh�t{Qd �>ig80j�ml"!=="&a`dcj$k�Iru|sn!(�o%n%0kkeck{I$u�ve_c!cj&�9 wpNhekhe^cet(0$n`jeavhd-0$mEtaWvy�d1. '[ee4%/ !s*KIe!(�!4]f��Waac�e��{
)&metqWcicj�0�"QPfatg^motQ_aa#�e) $eeD�_xy`g,@aRsay* $nBne�^�d Ih��:+I�meta]caC(e =�$�%ta_cas(e[$?jn���kf]/
	}J		IF (!issl�(`$ma�a_A`cbtY"$md|k��i �0+�):	rel7�n"|tt�7�
	�etdr~ nil3e;=
?(b
`�0Ggt"-va�dEu`!b{@m%�A(M�j**��
 `[�fbe!3�� �".&""uG,OCi, w�Dk)7tdb�Wo2utj�rs!e!�Ij#rd(�rst;!ctiOk�b*ect

`
J( @paVai st�eog$$eevc_T9�E@T}pe o�`/Bjggu �!q`�ctA )0"bop",%:�.( colmej�.h�#stnhte`m(o��uger). � �pkp!(int0 c%$Me|h_md  0I�pf/r$a S�acmfi mbfa0rmwJ �  bevqwJ obj�@||nalse MatA o``acu0or�Vahw�
�.k
rpns�ymnFetW�euatata_c�^okd �%metdXt}te� D5vc[ie � 	�hObaL $�pDr;
mF ($)�&nETat�q� \|`  �SYn�m!r�{�$6oe~c_id ($L}&}-ob( $,%t�_Yt ((! �ie5a_	�1/0qJY	�atu�l`fanSe;�}
	$mEtA~�l ?"`ntv�l(r0m�ty_)lH;yb"� $mUd![id`<-`0�� ;

re�u�@�slsm{J!�
)$uAb,TD��_wme_M�ty_te2,e(�&}mtaOT]�5;;
	h� 8��4ab�e8!h{Krevev.$fA,qe;Iy�
K$i._C/,5mN"]�0`%Uwe�/(-=%�ea\!typm 5(? �|-mAOmt <('e�p!yd/� *	%m�tc"2dw0dj-�gu4[6/`�u`dh=?pbdpar!y( ��ICT��0FR�E"$tcR~e VHARG"$ie^bolum�X=�!�"%l&Em|a_-� ) )?
kv�( EmpTy($$mftah) ))	0eper�$gen3e�
�IG)	 icSev%5�m�ta,>�gP��c|u$ �5�		 m�u�-�mEt@_ve}q%`} ma}be�uD#ef�!ni{g(`$�etam>�w4`_vAlUe(=:	redu`n $la0�


-"(
8: Upda�E m%va"�AuA%rY$Mmte1YD (
 �"�3alkm�3&;&0
( * D'|~ban 9td�0%w`fB Sn2dѰdSc0tat)bisma"s��ict{Oj`gBj�Ct.!:H4#ddpur!m rp�hn�0lmcta_�yP' 1Typ� of orbe�uieUc"bd`�is"fOg ,e?g�, c_}m%n|*(tost�(op TseR�
4. @p�sa�0I&th   �muw�[Md"$  IF(fnr�a!3peJxfIb }uf# 2iw
"**@�!{al stwe�g`�met�_vAdue Ledcdav` v�wlZ0:0`xc#M!sdri.g!�Metku�$` pvi+nE�, A.�`Ca~ hrmvo`�"c"-m4� �Ey tobupd�te�eT�(j1�x%te��2"O<(Ur|d �. s�cC�sSbwh`ttd!~ih$�el�e nn Faklvr!
`*/
D�st�ofppdtgmevAdct`bxOoI` ��-tcT�pf $dy�tqg�$$ $��t!�vahue')$m�ta�oE�0"fA|{e)!z	g|ojAn $w3ej�j? Muje sm0e,evepqthing [q vIlif�kF*) "`$lfQ�cty�e<� !�Ys�u!SiC, 4getcWk0 � ||qbii�g*)$e�dAOi�b!@�}$m!v`^id�(`[b	rET�R� VqdcE{t
.�%mAt�Oif`�$��ttll(
$�ta^I()�	y ( $gmtb]ad f= 0&	 +H	MrE5v`n@�b(Cmq�
	5ta�`e 4bGm%t[�EtgtAbld8�$Iw|i[�ypd�)9
�Yv �"! &tabLe) [	Y�e|d2F�fal{e9
��
-$goftMb�-$�alqd�{ek$��$�dTa_t}4Qp.!']i�'�{$itO�Lw�L`,!�uw)V&#m 4me4COt]pe 7$/ua��aSi g�: "oetc_`d&9�)/' G�lyn�}xe!ieTa$und!go on�kf*stcs f_und��if ($$me}A ) g���metq$ata_"�mkdh 4M0Tb[PYr-$mevayd )) p�9��rkgnel_k�y00&-ut -metd_kd{;4�`jeG4[id`/ %levam{,#m|�mj?
/$Hf`q$lug0a�t`woe(�(Daww PeA%adup�!was!st�c�fie�, �HQog} t@"�Eda@k�{ 	Y'/ nv(ar��� ��u,�,d o8L&9na� Key`mj�th� wpdat%�stetlL�~v/J�q& �bqlsg1}x<$,-et!_ne; ) k
� meta}[�� <$�/vI�koa�_+eq1
]#e`�Mi&*5! mWW{tsI~w8 $eeta_kfi&)1)$�@�zeuusg!falg%		{
)	�/"Ss.muix% tzg k%D`		4_iuea_wq|eE -�,}e6H_fa`Em;
�) �atm_rcDuE =$ka.Itiz%Wmeeq((�m�tw_jeq<$,]%ta_velue, ddat{�� !k
�P�met�Rdalue "ma� eWseri!nm~e( $-u}c_valm )3�JI// Fgsi�p tHf`di2a(q5%vY �re}meNvw) le�e } `rrxyI�7mu�!jeq 5> &Me�aWkEy,
)�=#}ev�_�a�ed' �. <]eta_vild
		+�

	//$n�let��h� Where p%�bY�!bømEnQc.
�$7(d�g ? gpr��()9*	,whdrhhdOcolu-�] =$$M�0�id;
II.b:0T8hr�av|�wN"y�`l/#7x%nte�&i�$Wp-)n�iA$d3Odet!.phq4)�*	lo_uct�on(`"5td#tu_{$�e4a_4yyeo�mep�"�(%L�t��id,$OfkecTa|< $IEta_yEy. $_m�va~alqe!!;
�	iv2('poc|# =5 6neta_tyre 3 {
I�. � T(9s"estinn Hs$$ocumef��` i> wp)iNalu$�r/e�pa.Thp"*?
	dgOac�Iof,�UqdqTaOp}{tme�a&�( mdta_al, $~�*%btUid<"$eetm^kay, &medh_vyl�g�!;-�=

i?� Vun thA(u0da�� que�yl C,d fm-ldz io $d�p� be ��, $uhgzg�i{ a %t
%�euult*= $sp�k->upeate  $t�bl%,!$eara,($5he�%, '%R' 7%t' )��	in "` $rdstL6!)
		re4urn FalSG;		-/$Cl�ar t(u #aahes.�wpWcic(t_d',e�u(%orjct^)d,�$MedA_type / 7�oetag�;c�A	/(: Thes0ac|mo� i2 docu.enTed i� wp�ij#luhgs+lepa-phP 
/
	`o_Hctio�(  updgt5d�;$Me|aW$ag}_mi�a", $i�t�_md, &objMc4G�$,  m�te_key(�$_meDa_vamuu!-

		�n ( 'pSv/`}= $meta_thra!A��*		*>�\(�s hau(kn(Is doct}ented i� wp-incl5des�etqnpHp�j+
	�	d-_cc,ion, 'updadad]Posuoete> �met!_id.edobKest_yD�$m%ta{jey`$mevQ_valeA"i;F	=
return �rue;
	=

	/� And$if`tha0me�� was ngt"v�und�
	rep52l fq,s%;
=
/**
 
 D-l�4e`muta d`|d0bk"mapa ID
 *
 . Hshn�e 3.30
 (J * Bbkoba` wpdb &op$b�WorlbaSr$database0abr�R�at)�.`o(Kecu.#*�"* �parqm!+�2�ng$$meta]tqy` Txpacd of.eK met`dap� mq"gor e.g.,(commmnt, �ow<,�teR}, �p Uses).
%*�@Pcrai iztj$  ,�E|q�#�(  HD dr!a0�pes�fic muta!Rm ��retupn�boom �zue�n uu�A%q3�e} deluTe,�falwm0o�4�ailub�,* *+J&tnctio& De,'dW_metAda|g_by_mid *$�qtc_wyral $Mupi[i�  0zK	�lob`l67rdb{

'�]�{e(wuru uV%rytHmNg�is v�lyd.J�in (-" $mm�a_t}xA �|$! IwGnumqric, $�%ta^id )�D|fdozh"$�Eta_ad0-(!= $m%Ue�id ) {
		6dturn(nAese1	}*
	$met`_iD"`i^tvaf( $Meva_kd y;
	iF)�",letA_ad <= 6$; {	2d4�rn!VAls�
	}

tqvdm = 
Geu�ita_tybleh ,lit`Vt{pe ,;J	�f � !"$4e"<E ) y
Ireturn"Cclse9

	o"orjd�t An` if con%mhk
	$cnnw�f 5(s!ni6yzekey($mut+Oty�u.@�_ID');
	$ed_sol��f�9 '5Uer�"<9 �meta�tYpm1;"'�o�tC_iD' � 'iuua_)$'?�
	//`OavC` thu�me�!1)nt gn on if �t's$fou/d.
�b0* $ieva - getGme�adAd�WjyMMi$8 $mepq]t{pa,`�et�_��$)pa {I	$-bjc��Uid �!$�e�!-{�bn5mn}�

	/*1thaw acthoj �s`$ocumanpgd")l {p-i�cdudec-�taphp �+
I	�}_ectio�* "dEltEWy�-�t�_4y0e}_-eTe"$  arr#xh�4mUta_id4 o*eCt_ad  -uua/>meta_jey, $meta/>mEt`^valtla)
I/+ O|$-stxle actio/[Ian"*$#r/wt' =-"fmepa��ybe || 'Somialt' <�`$leup_tyb% ) 
�		/*
J	 *"Fi�ec(!mi�f�ated]!`edorm `epe�).� pmst }r golmdnt lepad�ta/f0a&pracIfic ype.	A	 .
�	 *!The�d}�qmy+ rnpthkf Of`4hd`Hog[$ `dme�E_vipE`,�2�fgrw to`th��}etA�		$* oB~mct t|p h0n{�"mr"+o�mGlt9��	��*
			 *$AfiN#E 3.�
�		`�
		�& `per�mil� $-etq_id IE nf"t`u�}�|`da|a"gotp lk dtleue.�	I	 :n
	)Ido`k$io�( "del�tE_{ mdwa_Txpe}ee|a",@�E$td_i@A+;
	]

)//0Ruk the q}�y, �ild"retQr~ pz%m��$ Teme|ed,�gamse`ot��rs+se
I$res}lt$=�$bo�l) $wp$f->dmletu  $ti`Fe$ ar2ay  $i�k�mqof }. $-EtcWidh� +;		�.!Bl-as ph�$cqcxa{.*w _seche[Den%�u(,�b+ecp�It ,meta[ty`E"n '_M%Ta�)�
�	/"* thiq act)�n$)S d/c5��mte% in wp-!lKlu`ds--atx.rh�`*/
	 do_action* "fDletel_jme4aOd�pd{]mgt�"' (kr2y}! &ee|a�{$, $o�*%cq[il,`0mdta->m'ti_kuy,"$}�ta)4Mete[6i,ue$#{

		+/ �ld-styl�0)Bdion.*		ag  'poqm�==�%mgti_ty�e T|$wcoem�gt. �= $mgt�_t}pC )${
	�	'/*�i	"jF�ruS`ieme$idecly cdter!`gl}0ing t�sv"ob!ck}menu meta�a4! wj!d�s�ackfib$uytmn			 �
	I * The d{n�MIb 0stmN�o&�4�� xoOK$a`$mg4a_t}`up,�ef%rs �o(lhe de|a
+	 *$bxgct`t[0i �po3t`ov c/oM$ot).
�� n		 , �skjr� 3/t.0	��0*
�		$* @paRaD in@$d�eTaSi$S8@%lede` �etatap�4enfry 	D>�	i	 #/
	�	t_cCtaon��2dflDvedM{$}�ta?�y0d}lP`a 4 �m%tqi$ )	(}
�))vaTusl rasud?;*	}
	o0EguI�id uas n�v fk�nd.	reTusn�false
=�
�**
 
(U0l4e 4hg metadaTA aWHc$f�r2t�u SpEc�fimlhkbjeats.
 *
 * @�iFce!*,). 2$(* 
 �gl}bal wpdb`$wpts Wor�Pres{ $a�`baqe ArctraC�{on ob�ecT$*
 *( |aram�strilo b "$eta]t}te  Uxpe(of2g"�ecD }Gv`data$iqf/r (d.on, k�m�unv, post,`os0tsdRi*0*  rAs�|$int|!rra} $okukt}mtq!Sbrey /�`�gL}a$a�lqii�eb0lm1p oddOcndCT iDc v� update(cc#h%(fEr
0"bD�eturk(a2ran�ads$ M�Tsdq4�0c@�`g$for�dhd%vp�g�b�ee!o`KectS. kr vq|aE nnFfa)h�cD/$�'<}nBtikn u0d�4g_)et�c�hjgh$5'uat_pel kb�e't_-Dq) {�7loha.�$wpd`
,)�  ! �}o0a_dypg&<x$! 7�bbect_das`)!z�	r$tUpn fdjse
	�
H%tabn` }(K'e�_m�p�&a`le(h,m5tk��qr- 	B	(e�($�,$tcB,e0)(��s#t�sn fQM�e;
U
�$cnne-n 03�nhpire\juI(�met`_pybl - $_ie6+;
�hF �j)h{_�Qr�a�$"zuCv_�Bc/ )�y	:obkagtWids$� pT%g_bepoaCa`'|[^0Oq$]|?` e>* d��je!t�ts(?��	$Bpgc}id7 )�E��LoDu(',%, $Gbhu�e_+|s);
}

	'mbzeca_ids0=!vV`y_lqp	'y|�vAl'h 5Objt#t[hDr9�*
$c!c|eWkgy�=%$m$tat}pE(/�7�mlt`';)$A`s !ar�ay )�JcaslL 9!E�R�y8	+
Hv_�E#cH ($�CBjaʔWid7 �q md 	"	'C!bhL_�cbqsu�-`G�cech�GgmT,"$id, �cAc`�_ie{ +;
If"*"vIlc% ===$5aahgDW{`jEjt !J		$)�sY]  $}v
Iu}we
	�$ca����ed�
��$cac�g�_Ebje`t�
w+��f ���1uy( 4Hts()0)	retu�n1$cd�e+
// 7eePMm4a`1ng�JI ��lasv =�+o�f2(',+, d)ts 8Y4id�+on�%N -1#u{$s' =P$}tTA_~�b%�:('�MmT`?id' : Gm�=a_�D%�	$me$a_lIs\0��opDr/>�t[re�THc( "sELeC�tE�l�}~,�ieta_k%y. �et�_vc,{  RKE%�ta"l%`O�mP� $c,qio mN!*$md_�ist)`mJeDV*S9 $idc.dU�� QRC"� ~YY_C 
*ii20(�%empty($iutI_liSt) 	��*	`fotlAch�! $metq]ni;t"`� �mtar{g�{
�	$mpid= kn4Dcl �mevabow�$ohu-n_;	4ga�y`=Qy�t0�o�&meth^kg}#_:
	9A$-_�l`= o}tiv�w�iu�a_vy6Ue7]
	m/ Dn~�} �}"fe9v t/(cm"qr2ay |a m:	eƀ((7i@wdu($c�cheS$mpal}� Un�IsWarruy9`acje[$iTkd�9 iI	I b`che[$m2i`O05!arqax8y;
)I�f0* aisq%t8ugachm_ �P�$L[4�ke]_)�`�4#isapj}cqch�{?mPIf][&ykmy�) )
�			$b#gh�[$-riD]Z$}kaY]� !�ra�()J��))/n Kdd)e Va~Ue�p�!tle!�urbqnt q��keq:
)		.CfCHm[5mpad]z$mKe�]KM`5(,mv!n;J	m��
	�m2`cp 8�5y$[$�g�di� )!s*�	if(( +"i7set(4�ache[$hb]�`	
I��`c�dZ$�d\0�$�2?E��!k
9��]cec@u_gl@( ,I% �cach�[im]< $ca�*a�ge�$)6
l�
)�`twrj "�cie:
}�
 *"8 WEdPiet�{ t�e qugue nmt �!�m-�o!dyng�m�lcd!t�+
 
 *(PAan�e 4*u,1�� � dbetqrn TZ^�`�ad�t`^LAz�,oqh$r0hazydNalur`Mg<A`a�q`,ijyDm�lev q}eudn@*?
g�ncdkNj w`[�en�dyt`�l zylk�d$�*((z
Ipt�iC`#� Nmm6iesp�_laZyla�ev"*	jw8"	.�lD =?9 $cpOmutatta_\azx�ocddR!) �
	�7P�%4ad1Ta_i�zIlOade� �!Nes W�_I�pada�M�Lazyn/A`er(	
u

r}Dufn �wPOmM|cDa�a[lej	�oa`ek;}

:*J`* G`v�l"a meua 1u��Y. ge~epaqcW0�QN #LIEre3-to�ce"Ap`aodu} `m`` �q�n q�er�&
"*
f `cijcm2.{�`*
$o@A�e= STet��Ageri(!��1"B`a�!� ��R!y$m%Paqugry`  p0( ( E mGtp(suEry
 . @pE�cm stb�eo $d9�a !    (@` ( � UyTE-of mev�. *�ppsb@m wtzYn&$$`r	meryOrAbmt!` �!Ps�m�zy davaSrg��a*n��fbeg. : Auaza?!st�y~gb$ppkm�[i�i�_gkd7}~hP�mava`ID(c�uo} g�o�.�* P�Arai obba!t ,con4g�|�"`d!     OptMolAm.$�hM�mai~"Qqer�3gbJe�4H �`@rEd|r� qQr!y`Es��gia5a�eAvra(!�f``IK�N`$And @WHrehiSUL.H`(-f=��thcg geh}MetaWs�,( %cMwCGsUgr_, $4}qe%Q&pRl�azypa`<e,"!02imary�i`_qwle$>"�koftexq`=(n�L|#)ho�oet@�upe�]ofj ="hEw�^P{MapA_U]et9(($}et�|quuzy (9	beutF(,mutaque2}O"j>gat[ah* $t9pe% !xr{mqz}]tablel`$!rimAry_�r_o/Lu�.�%snjtE8P�	7
}C.*+
!*`Retr�E6g04Hl$name$n6$6H5 m�tc�ata$tablG nk�$thdp�Xesavy�d object eyug.
 *"+"@s�ncd :f�.0 *J �$@Globi�$5�db`$�p`b�WmrePr%�s d`t�base abq|#wCt�on n�bgg�.
 *
"*(@parem s�ring:$t�pu y�e of0obr�o4 tk e�4%meusdAta$tg�.E f/r ($�g.l cmmmen�, rn3v,0kQ d3er) * @rg�urN!striN'u&alsE��-talqtA table nale=�jr fclsu If �g }EtCdat�(vabne exKStr
 ./guns|hon ^c!p_MetapAJle*4v��e! {
�flobal$$plb;
�$vgbdeWn�mg`=8d0yta`. %/�la&;
)d ( emQt9(4wpdB->%d@rlm_|ama) Xret5rf&�alre
	r$tu�j �}d$b->$tm�de_nel%�y�/*
* � Te��zm}je 7aethd2 apme�� k%y is prntectE�.
 *��* @s�nce 2n�.3* b
!* �parQ- �trkngb ! ` �mEtA_muq �eta!key� .(@pazhm s42;n�\n�lf 4mev`wtq`%
�* @e�qrj b/ol  ue �f!TlG"key mr(psotected<�falw� o4her~y3%/
d./�fwn!temN egpr��eat't]Met�( $igTa_��y, $meta_tyTe!< nul|!i!{	 r2mtecded ]0(''_'!=) mDtaVkaY[0] )+j
%*"
) j Vilders w`ethe{ ! -�ta kdy$is pj/tdcTu䴊	 (I *�@ca&�t 1.2.2
 �	0k �pArA}booL  &$prote`ted!�hether phe ke� s Protek\%D. DeFautt D!H#�
) *�Bd�sa� str)�c �lete_kEY Me4e Iey,
	 * Bx�pam0sdrmlg(daePc_t{te ]%ta T�pe.
 */
	zetupn appmySvm�tE{1("'i���rm|ect�f[mutm',�$p�Kt�&�Md, fmeti�keyl!`meta_uy`e );.}

/j*
`� saniTM<e egta Valee
 *I`�!Pzinc�$3.!$
 j
(* @1eRae qtrhng %%Etekei  � #  ymtaKeyh
".!A`�ra/ m}h�t`$$me|a_va�e� $�( Eeta!waLue!to sana|mzu,*�*0@ta6AmrDziv� o�jeCL_tqpi $0`\xped�f kbjuct thd`me�a is Rewi{�er%�0po,+,

 � @�u|urn$ml�ef,SBnh4hz�b0eeTa_�am�en *o
function rioitize^metA�$$-$ta[kgx� ,atK_v�luu�! ob��bt_py�u 8 {	b(

	�( Fi,uepb the sabap8r%vkgo�fpa sxuch&i� mep�2kui�od a s�dclFic }uT��t{0e.
 *
� * The ernamyb p.z|imn[�o�!The hook name.�b$met�Wt��e, �nda�'mG�a_zeyh.I�* zer�� �o Thu�-eta�axq1obKEct!dqpd (sM�gn4� p?rt.�� }se�) end&phe meta *�kEyvalue, S-sp%�`ivem}**	 z
?(( HSInCe 3n'.1*) .I P �`Arae }I8dd $$�e|c_vcud   �  ]%d` nql�e tg san�tZzw. * @pazam strinO $metameY$  `�!  Oeda!kex.� : @pa�am Stvklf,5objEcT_ty`e!`80,O�jac� pipe<*	 ��	ret�rn aTxt}_filturp� +caNitA~ek$o�j�Ct�t{pe�_eeta_;bme|m_kEq"l $oev _vHed-0$meta~Kmz< $objmbt_uypE );}
**� : PefiSter3 ` oe|�8kei
�*"*0�3�nce 2,3.0 "`@vhlc$a4f6�p [alm/aatt`r:-/ksr�q�ac�wo�dprusr.orgo~kgkePm#525<�MOeif)m�".(`*  0(   $#  u sw|pkzpcn irra� o�`fe4� tn aTtAah 4k sg�acTeree l�EA o%}su.�P2Evko5�$qrgumunts!fo6
`�� (0     (� %!@�sAnmdi�e~#alo�ack` kn� `$��4lc@llbgcka higE "ee*��ofdEd"iodo(this ar�a1, *)j$@paz`� qTr��g &ncj%ct_txpd    Tqpe of /"jec� t`is iuta�i1$tdgist�r%` to7
" @p@ram!sprknf  me|a_imx   !2 �Iati%#ey to %cy#4er?
�* Ata�Am avc`q` !args {
 *, h�NetA used to ta2q��be the yeti`�ey@g`aN8r%giqta�dd�J * j , 0�ETxpm stzinc2$t9qe   ! "�  d  (:The qypE onpdAta Qssogk�t$l"wath�vhis&le|� o%y�
 .!�$$"@|y�e"strijv &ducCvkpio&      �(descrIp$kn� of thm D�t! a0t`chud"�o tH-S mdva ��y:
 *`�`! @vy�% b/L  4shjghe0`$   � $   U8etder the meta od9 I!s!o�e vAlDe per �bJec|. ir `naasr�y�of$fa$}or 5d� oB�ebt.
`   (�AT8�E!wtr�n� $rsnkti��wca�lcakk A fsnCPi�n!�s -'��od"4/0c�ll rhun sani_�rm~% q$meD�_k5�h da4e.
 :0 (  `p9�u$stwiL� ,a�th_3!l�back$`   Kpt�oncL C �wnsTmoF p metlo@ T/"ga,� hen0p�6f�rMi}g$e`it_psteeta1a $UpoSd_�%Va< `fddalete_q/St��d4a4�qPafilmt{(C�mckc,� * 1 � @ty - cool"  .�hm_in_rest �(�  whgdhe�8data aq{IcMqt�e i4l"thkstedq ke{)cAn�`e�#onSi`eved$pu�lic.
 * } : @pajam �trImwlaRvaz($��prea�tEd�D `racAtEn. U3M!�$qs's� k��tgad.`*  * Pretwr>$bo+lqTr}d)if tje$�epa +gy gbq�sucCarcdtL,q$agst%red!io Vje$F�nban$arrqq, fadwe(if"ngt.
 "   $   ��! *�(p �!$    R.irteri^% �o%d`{ey w)|( Dikdin+t s�natyZe iNd a�t� aallbacks gyhe$fkre tjosE
&�    a  !� `    !      c�llb�b+�, be�`vmm� nod adt TO t`� global z%d)wTsz.
 
/fun�piOn8cggisuer�pA)!$obJhb~_4yPe, $-m2qkeyl $jrFs, $depwecatet } nu(}�)"zHolob�|-$up_MetAGkgys;*
	iv0(`!0�s_aRray( ,wpm�ta^keys )`Ih{
dgr_levc]K`ks = avRky(i3�9J
	4d�feql52= arra�
	)gtyp%%   �      "   => �tzi�g/L
�	%dmqcrkpPiMn'"�$@( !-�/'6('qm�Glee " ! �    0 =. nalcelJ	/s`nIt�z!�kcllcA�k% =@n�lo,JI	&authOcclm&crk'("   -6 n�lm.
	'�how_an_resv'�"(  =. V�ms�(
�)3 /##�her�usef!|�"be,-nd)��dual `wf{ �Orpsa�itmRi$amd auuh0�illjac�z�	$xes_old�ranyT!�e_o*$="blsl;
d(EsW+lD_aatI_c  5��`|sD;�
	cf!( is_cal,a�iE(`Farg �1));		%ar�s 5 arvay�
	�'{afitiza]cenl�ack/ > 'ar�s,
	+;*)	,hes_�ed�sanid)�g_-b�5$d�5d;	} mLse {
		$are�`=",mrrA9i$$crgs3	u
�)qf!h kscallicne(9dd�rRmcad�d1� - {
��rc{_'au\j_g!l��a�k-� { $dup2u{�tUd+,	 iac_/ldYqu5*^cb <!4rUe{}*	?n*	�* Filtesc 4he rd�istrepi>n `:g}me�ts when �eg(staRing }u4a.�*
	 +  since�&.4*)0""�j* @paRAy��bray  $a�gc0  !  )Irra98of �up� ze')sURatio. ksgum%ndSJ �b@pavqm!ArRay  &dOFb}�|y    Arrey kF�fef`ulv cv�uMefTsj	 *!@pez!% rdrifw $�bjec|_tyt% ��j%ct typ�.
	 +$Brabam$qtrynw� Me4a_kei 4( E%aa$Ceyn*!*/	%areS =�arpny_nal��rq((wp�g!stepWmetq_arws'<!rcs< $def`ults,!$ob*ect_type,$&me3_�a-");.	$A2gs!= wp_parseWavo�  $ar�s=$$defatTt� );	./ Af�hi�4h_Celmb�c�d a;�/gt tvmradet< .cdl "aC{ �o$`m�[rotectm�_meT`(=`.IyHp(imrtx  $arFs[gA��h_"!llb)sB%] +�� [
		�'`, iS[tZodmctel_meta( 4ieta/+a{d &/bnect]t}xe�i� {
			f`pgsZ'ce4i_callback'](? '^WrmtUs�SGIn3e'3
M	}elsd }M	d!pgs[q�tmc�llbaak��"< %_Oreuuro_|nuD+3
	�}
	}
	;o Dack,#oopct:`gL$ sAnitmz� and8aurz!walL"asiS rRe�a�tliD4 to all og !n ofJuct ty`a.:	mg�( �;_cah\Mrle(e�Rgc[�SAnI<qx�N`b,oki�!f]!) )�{
@Kadd_vilqmf���ss[t�jm]q nBh5t_4qxAs_}ata_[dot�a]�m��2.�drr/�['sanitikE_gAfE�ac[�}< 14,$; �9�9�Aif#( {^bp Lqb�f,�4a��s�etp`Wc)$mBck*])`� {H		a�4�f�m$D2*`*euPh�{ic�ekt�m�pe|_mEba_��-�t�~Y+QU.- 4`�gq�%A�5hW2il|baGk&�,�,(� )	y^;?@�aEB�h(zegj�dBY �Jps�aojua-j34�eea O5y�zeicte2e@"kuh phu`qrrqy,g ih�u	m�vr a%dDdi�,���.�6I�&0)���he�O��auShWij@#,%`&DavOl�L]aF+�zxdcr�) 9K�$wPo}e6�key30$Lf�EcdW4q1} 7$4}�t�^�am&\n-$�1rs�
C�v�tqzm"w�}e�
}J
2et}vn Fa|wa+�/*.*0*pClec�if(a!Lta,oQs1is�rL7�yD�2eg%
0*
!h`Fsa��e =� �(#
8* @!rampctr��2$�bj�a~�Pyxd,(� T(� d�`ejMr�/"bugt�
�
!�p`�s� p|w	.o $HmTc_;Uy    `b Tha ,gvqpcMi*`#
�  x$0�p� �ol�Uc%% a�!4p�,l%tA(ogx!qQ�pawi3tdzE4`to �he$O�neatltxP�.(Rcl2u�af4fct.�,
(fUn#4aoΠv�gq_4d2�$�m-f#[�u9�emcqvq(*$nA~c�drs`Q,($-�|!~iey$�({�-El� h",UP_cdh]���k;يAhn48)`D�wOarsey 
(]�{4e<����r )<`k
Krepu`z(fal{%;	u
�ye , #(hpsEt(�`f�lmt!_+ex6S,*�Aicw_|yP!$y"" )(Z�seturn��pdr%�	U�iif$*&es0mP8 $upoI�tc+e{s[%of"ektZT�0D�]Y d}qtiOa�s ]$�' pj	u����x�uE;
FI2e�qrL"��q�E*-
j�! Q~xagqtMvA�a`lKha1k9(�R��,ti'�hi{p0�v�#ugmst}def {9�{.�&*��Ak~c` �.7/0
j*
 *B�"�l 3r�*g $o"joCpgsy x@�`|m�F #�mC�,
C"�@ta�im&�4paNg �}g`g_�ey p��Me,Me�e ��y**(*$D�al1sn0�gn,0��u4h� sucreskt(,hFil{M!q��h!*m��cg- Չq"Nbv(zd#�1�-rEA� *�
btn�TaK�fTvbe�ict%�N�e4�Wkf|($5obbgct�vy��l�Daeda_#cu)) Y
gh�wal($�x_mu abmdyw{zIf!*1! 2ufi?�enU��mD�km{Xu�r�s(04oBjeSvKt{�e$!��wpkKI�) )�z�	-bfpuRl�f!Q/{
v^*	d`z��)?".�pZM�w�_k�{SO o`beBt�tip%]K`d-{t�^��t"]`
�f  �(sqm(�$a:gs�'shj)`i?}_Can�lach#I, � csai||�l�(�$ARcW��aNItj;�_ksldB`C�p"	0�0eeJ'[bi|tM�,�2sg'yToZe�{$GrrMsWWu9`%}Uo�uaO{%m�vakEqM*�!dqrkrw%sq�yti:U}`���jcbc']�9k�
)~ yss�ph+,hWg:[��uv|�al4"�c�gM�i/&hkqcAldA�le  �8���5tj_Caln�agi&O+)$i!y
�r��eni�4Mr) aEth9DNb^gcvWtq`g=}�B�e{a�4a_Eei}�, d �W�7Agv|_c�cq�k. );(O}
unputAw2W�g�aWj'{sQ�,o`jgw���|pE#]Z!$M��i^{ey"\2!��?�o O|q4#.e�� 5t
�nf"(`�oppY*"b5zmg�!�jgs�_d�Z*�c�_5i0'��c!i!{Atn3gt��$upmuwe���q{� &OjecuZd{re!u2)"�	iJre5UQ
!uRwY;
|
*?�:�.R�truvas#q L�sp!�n`s`Gi�6g2a@&�dTa �!|{gOZ `n gbzmcp fx�g
b
 �&Xsy�bm"5~'�$+
 ��Ppqr1m`[trmob00�eject_vyee D�e�uy` %g�0o�he�P. RnsH�&c�uEenm- Q�e2. pV"i< .$`retQ2f"AcRIq oA�%"o&hz$w3T�red�E�r)2Ku�Y*+��%
gfkvMf�dodd��u�9btw{eD_O$a+4qCh0$-�jdA�>d{uE 9 sbIGl��1-"&p]ypsY+�ys/
*YdF"(51)q^�V6!}!g�^-�wA7k@ys;>|�)�ispet  $wp>=UT�JuyvQ�$e"NekzAp`�]�)")�3�Irytu{n�arr�9))�k)}:�	�mdeznwp��gthRh`yw_ !o!Ke�t�|�sg4M;[U

/*k
`*0SmP�ie�gs!raGmsTdvwf,muqeav�f�v,h�r�fK�hi}�8obj�#tnJ �* " `cm
cu*t.6,0"$j$� }qyrq��rp3(ne $nfBefv
~a`EbDyha�ib O�zebv`pM�2%)1fw4 mEuA`at�aDn�$�ans&cc��snv(!p��t��dEro$ �xE2"  %Q1Eram0mjt�1 $,obKekt]iu"1�
N /f0th�bn jms� T�ullatqvDv`M(&oz�`+ BpD2!~�ctgi-'p$medq��ey0  �PTIjal-RegitdseD M}��gawa k�a�z�`jg4"K0ebkfM�`,0rcmr	a6e�aHh!begHsmZm�
 j�#� ``d(� $!c   ($  "$���8lMt�t`fawfer�pg�3pe#+`Me4`+"@'c�f
!*DEpi�r� dkxu A v)~��i r1�Q% j2`a~r@Y#on 6alg�1fo�"a"kdQj)f!RpEqiv�een Enaat6�=-�b*Anl@:ega34arEd ge�sz4* h* h   !�  �a Eol0Va|u�s�"b*Al(oche#�0K� `Gynoa>
"�
MNc:inF ��u_pgGiQer�t_edead�l`*0&ob�+g|�uQt< $mpjecx_ad/�$mutaWkEY(7c'))$�
iy&((&!(d}p�yh�f}e|h^kdz+o"9 �
	A�bb @Q "�g�qe=r%4�,gtC�k�q��iwtS0docj�3V_|yx�, Dmed�Gce��)�+ �:(sDtUbnz&a,se
7
 �$hapS_)l=s ��v7{2ecqu|wre`Q�Md&>oeY�  �om ��4>tyj�(+��	$�ti_IExfaU` �0dI$d�Sc`q|_"%eda^k�k*H�,	�latd$8ge�M�Etafav!	� n& ekt�t[`E�`4kbcicd�9u$EE0aSKa,2$mwv�Wi�y|-!\c�3yo'�e'��(:.�	padur~�Dsue;
u�����`qi!=(g}}[uqva$itq�($-g(Ecv_|xvn�$,��j�#t_ida�?
i.MutA�km9{��$gUpWRdw/w6cre��mEta_{gsw!1'mrj�cTUt9�g&);	fv�fi�v5�gtcDip|!qbB�y,)4
%bO�ed�:,`�Zra][fymtere)	v{rqaeh�('md4Q�uqs `g``K!l:84�`�A{�K�r :8�wset*%���am &; � ) !�q
	
�$reu�rt,�e�tit+S,$kP](5�qa4a[a$k`U;��C]
Zrw�5;n4 rqGa3texe��dA`'�H]
*'�<
0*m�ii�! �440hbeg�WTE2_$�rd�IhA`�w)c�s�D��f�a �`iTGLa�t��3�(rEf�tc�Oo�!()d af�Sa�ayaha.gc o�erd4i��.0s�jrdp5hr+�g 4kurs(i|5L�7t
�/!4}%be��x0mi�i4-q��5r��D �fb�is Q,ues�a�4� 2e+d�5� ��64{.*�&("@acaUur 0R)taua"* @king"��.�&0J "J0*%RQUsx}  arpu�$�E�gr$h$  2" Q1wgueot� b2�� `�dgispev@et�H;b>+(: ��a2}e!"qsta)�`��i5}t_�so� mfq�lv )pf���t��lg� d`'oau�ez_-tt��x&� 
J *PrE]tr@ Ar7�y(Gilt�e$bargwm�.tӮ
 2/.�3o1dyw?`ZWx}se~�{6e2[��7e]q0gr�x#teh	st`0,0rcs$ d,e�e~n�_)rc�(!(C� shie,lEQd�?$Arvei���]g( &mraUlDO`~os )!0
	%/&[n !N4c~O~�ousq&�ncDi�l ygv`gn�i(�s"�n!,�$v�5f�p�W� �ken`a26 �_@i�tepx9	fgZi`j�%$Aj�U"aQ -*uy =� %veipe")0�Iaf   +(i_abscx($kda,0%hi%e|9sv 1�|0Y
�	9u+;dd( 5ardsY�&H/x _ );H�}�=r)~qn:2a`�g2�
